export const environment = {
  production    : true,
  apiEndPoint   : 'https://csapi.dev.claroshop.com/caja/api',
  apiKeyCaptcha : '6LeSImIUAAAAALK_gdGEKHGIDtIQd_07Hn7tOk42',
  baseUrl       : 'https://www.delta.dev.claroshop.com/',
  cookieName    : 'token_cart'
};
