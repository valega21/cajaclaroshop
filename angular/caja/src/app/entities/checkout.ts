export class Checkout {
    codigo       : number;
    deviceFinger : number;
    status       : boolean;
    error        : number;
    message      : string;
    action       : string;
    data         : any;
    attempts     : number;
    token        : string;
    payerID      : string;
    url          : string;
    transaccion  : string;
    cupon        : string;
    importe      : number;
}