export class Promocion {
  nombre      : string;
  pp          : number;
  sec         : number;
  meses       : number;
  mensualidad : number;
  idFormaPago : number;
  checked     : boolean;
  datos       : Promocion[];
  active      : boolean;
}
