export class Cupon {
    status       : boolean;
    error        : number;
    message      : string;
    cupon        : string;
    importe      : number;
}