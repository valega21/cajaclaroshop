export class InitResponse {
    error   : boolean;
    status  : number;
    message : string;
    action  : number;  
}