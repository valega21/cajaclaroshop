import { Direccion } from "./direccion";

export class Multipedido {
    idCliente       : number;
    nombre          : string;
    idFormaDePago   : number;
    idDireccion     : number;
    formaDePago     : any;
    mensualidades   : number;
    total           : number;
    envio           : number;
    attempts        : number;
    clickRecoge     : boolean;
    sucursal        : number;
    nombrePromocion : string;
    facturacion     : Direccion;
    montoPago       : number;
}
