export class Carrito {
  idCarrito : string
  token     : string
  producto  : number
  cantidad  : number
  tallaColor: string
}
