export class DireccionTelmex {
    id             : number;
    nombre         : string;
    pais           : string;        
    estado         : string;       
    ciudad         : string;
    colonia        : string;
    codigoPostal   : string;
    numeroExterior : string;
    numeroInterior : string;
    telefono       : string;
    municipio      : string;
    direccion      : string;
    contrasena     : string;
    action         : number;
}