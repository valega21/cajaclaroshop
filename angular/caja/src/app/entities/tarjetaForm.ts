export class TarjetaForm {
    id          : number | string ;
    nombre      : string;
    numero      : string;
    vencimiento : string;
    codigo      : string;
    tipo        : string;
}
