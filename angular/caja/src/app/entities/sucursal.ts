export class Sucursal {
    id        : number;
    nombre    : string;
    tienda    : string;
    direccion : string;
    horario   : string;        
    lada      : string;       
    telefono  : string;
}