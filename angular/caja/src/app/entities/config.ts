export class Config {
    apiEndPoint    : string;
    apiKeyCaptcha  : string;
    baseUrl        : string;
    cookieName     : string;
    cookieLogin    : string;
    tokenName      : string;
    tagManagerId   : string;
    facebookId     : string;
    emarsysId      : string;
    codeName       : string;
    crName         : string;
    secureKey      : string;
    secureEndPoint : string;
    carritoEndPoint: string;
    t1pagosEndpoint: string;
    t1pagosToken   : string;
    t1pagosId      : number;
    t1pagosAmex    : number;
}
