import {T1Telefono} from "./t1Telefono";

export class T1Direccion {
  linea1      : string;
  linea2      : string;
  linea3      : string;
  cp          : string;
  telefono    : T1Telefono;
  municipio   : string;
  ciudad      : string;
  estado      : string;
  pais        : string;
  referencia_1: string;
  referencia_2: string;
  longitud    : number;
  latitud     : number;
}
