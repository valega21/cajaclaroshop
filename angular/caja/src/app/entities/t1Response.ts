export class T1Response {
  status    : string;
  data      : any;
  http_code : number;
  datetime  : string;
  timestamp : number;
}
