export class Captcha {
    created      : boolean = false;
    status       : boolean = false;
    delay        : boolean = false;
    attempts     : number = 0;
}