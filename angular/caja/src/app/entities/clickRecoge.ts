export class ClickRecoge {
    producto  : number;
    sucursal  : number;
    cliente   : number;
    skuHijo   : number;
    token     : string;
    status    : boolean;
    error     : boolean;
    message   : string;
    action    : number;
}