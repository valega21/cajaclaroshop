export class DireccionResponse {
    id      : number;
    error   : string;
    message : string;
    status  : boolean;
}