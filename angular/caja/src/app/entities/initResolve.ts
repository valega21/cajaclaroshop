import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { InitCajaService } from '../services/init-caja.service';
 
@Injectable()

export class InitResolve implements Resolve<any> {

    constructor(
        private  initCajaService : InitCajaService
    ){}
    
    resolve(route : ActivatedRouteSnapshot, 
            state : RouterStateSnapshot ): Observable<any> {
        return this.initCajaService.initCaja();  
    }
}