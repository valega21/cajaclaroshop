export class TarjetaResponse {
    id      : number;
    error   : string;
    message : string;
    status  : boolean;
}