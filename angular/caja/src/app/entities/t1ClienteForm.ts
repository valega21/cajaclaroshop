import {T1Telefono} from "./t1Telefono";

export class T1ClienteForm {
  id_externo         : string;
  nombre             : string;
  apellido_materno   : string;
  apellido_paterno   : string;
  sexo               : string;
  email              : string;
  telefono           : T1Telefono;
  nacimiento         : string;
  creacion_externa   : string;
  device_fingerprint : string;
}
