export class Emarsys {
    id          : number;
    title       : string;
    imagen      : string;
    precio      : number;
    c_descuento : number;
    precioLista : number;
}
