import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ClickRecogeService } from '../services/click-recoge.service';

@Injectable()

export class ClickRecogeAppResolve implements Resolve<any> {

  /**
   * @param clickRecogeService 
   */
  constructor(
    private clickRecogeService : ClickRecogeService
  ){}
  
  /**
   * Resolve
   * @param route 
   * @param state 
   */
  resolve(
    route : ActivatedRouteSnapshot, 
    state : RouterStateSnapshot 
  ): Observable<any> {

    let token = route.paramMap.get('token');
    return this.clickRecogeService.initClickRecogeApp(token);  
  }
}