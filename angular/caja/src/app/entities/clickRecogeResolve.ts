import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ClickRecogeService } from '../services/click-recoge.service';
import { ClickRecoge } from '../entities/clickRecoge';

@Injectable()

export class ClickRecogeResolve implements Resolve<any> {

  /**
   * 
   */
  private clickRecoge : ClickRecoge = new ClickRecoge()

  /**
   * 
   * @param clickRecogeService 
   */
  constructor(
    private clickRecogeService : ClickRecogeService
  ){}
  
  /**
   * 
   * @param route 
   * @param state 
   */
  resolve(
    route : ActivatedRouteSnapshot, 
    state : RouterStateSnapshot 
  ): Observable<any> {

    this.clickRecoge.cliente  = +route.paramMap.get('cliente');
    this.clickRecoge.producto = +route.paramMap.get('producto');
    this.clickRecoge.sucursal = +route.paramMap.get('sucursal');
    this.clickRecoge.skuHijo  = +route.paramMap.get('skuhijo');
    
    return this.clickRecogeService.initClickRecoge(this.clickRecoge);  
  }
}