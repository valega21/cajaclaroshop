import { Component, OnInit, ViewChild } from '@angular/core';

import { Direccion } from '../entities/direccion';
import { DireccionesService } from '../services/direcciones.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { DatalayerService } from "../services/datalayer.service";
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-direcciones',
  templateUrl: './direcciones.component.html',
  styleUrls: ['./direcciones.component.sass']
})


export class DireccionesComponent implements OnInit {

  @ViewChild('detalle') detalleCompra;
  public  ruta          : string = 'direccionActiva';
  public  title         : string = 'Elige tu Dirección de Entrega';
  public  loading       : boolean = true;
  public  id            : number = 1;
  public  initialized   : boolean = false;
  public  config        : Config = new Config();
  public  url           : string;
  public  idDireccion   : number = 0;

  /**
   * Array Objects Direccion
   */
  direcciones: Direccion[];

  /**
   *
   * @param {Router} router
   * @param {DireccionesService} direccionService
   * @param {ActivatedRoute} route
   * @param {DatalayerService} datalayerService
   */
  constructor(
    private router : Router,
    private direccionService : DireccionesService,
    private route : ActivatedRoute,
    private globalService : GlobalService,
    private datalayerService : DatalayerService,
    private configService : ConfigService
  ){
    this.config = this.configService.config
    this.url    = this.config.baseUrl + 'carrito'
  }

  /**
   * Hook init()
   */
  ngOnInit() {
    this.getDirecciones();
  }

  private setWorkFlow(): void{
    
    if( this.direcciones.length <= 0 ){
      this.globalService.setDirecciones(false)
      this.router.navigate(['/direcciones/agregar']);
    }else{
      this.globalService.setDirecciones(true)
    }//end if

    
  }

  /**
   * Obtiene las direcciones por medio del servicio Direcciones
   */
  getDirecciones(): void{
    this.direccionService.getDirecciones()
      .subscribe(direcciones => {
        this.direcciones = direcciones
        this.setDireccionPredet()
        this.setWorkFlow()
      });

  }

  /**
   * Elimina una direccion
   * @param id
   */
  delete(id: number): void{
    
    this.direccionService.deleteDireccion(id)
      .subscribe(() => {
        this.getDirecciones()
        this.detalleCompra.updateData()
      });
  }

  /**
   * Establese una direccion como predeterminada
   * @param id
   */
  default(id: number): void{
    
    this.direccionService.defaultDireccion(id)
      .subscribe(() => {
        this.getDirecciones()
        this.detalleCompra.updateData()
      });
  }

  /**
   * Establese una direccion como predeterminada
   * @param id
   */
  envio(id: number): void{
    
    this.direccionService.setDireccionEnvio(id)
      .subscribe(() => {
        // DataLayer Direcciones
        this.datalayerService.setCheckoutStep2();        
        this.router.navigate(['/formas-pago/guardadas']);
        
      });
  }

  /**
   * Define la direccion predeterminada
   */
  private setDireccionPredet():void{
    for( let direccion of this.direcciones ){
      if(+direccion.accesibilidad == 1){
        this.idDireccion = direccion.id
      }//end if
    }//end for
    if(this.idDireccion == 0){
      if(this.direcciones.hasOwnProperty(0)){
        this.idDireccion = this.direcciones[0].id 
        this.direcciones[0].accesibilidad = '1'
      }
    }//end if
  }
}
