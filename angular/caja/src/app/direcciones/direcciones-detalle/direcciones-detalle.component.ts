import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Direccion } from '../../entities/direccion';
import { Zonificacion } from '../../entities/zonificacion';
import { Validator } from '../../entities/validator';
import { DireccionesService } from '../../services/direcciones.service';
import { ZonificacionService } from '../../services/zonificacion.service';
import { ValidatorService } from '../../services/validator.service';
import { DireccionResponse } from '../../entities/direccionResponse';
import { GlobalService } from '../../services/global.service';
import {ConfigService} from "../../services/config.service";
import {Config} from "../../entities/config";

@Component({
  selector: 'app-direcciones-detalle',
  templateUrl: './direcciones-detalle.component.html',
  styleUrls: ['./direcciones-detalle.component.sass']
})
export class DireccionesDetalleComponent implements OnInit {

  @Input() direccion: Direccion = new Direccion();

  public url      : string = '/direcciones'
  public title   : string = 'Editar Dirección';
  public loading : boolean = true;
  public colonias: String[] = [];
  public error   : boolean = false;
  public hide    : boolean = true;
  public mensaje : string = '';
  public default : boolean = true;
  public rules = {
    'nombre' : new Validator(
      'nombre',
      '',
      'string',
      'El campo nombre es requerido'
    ),
    'direccion' : new Validator(
      'direccion',
      '',
      'alphanumeric',
      'El campo direccion es requerido'
    ),
    'telefono' : new Validator(
      'telefono',
      '',
      'phone',
      'El campo telefono es requerido'
    ),
    'numeroExterior': new Validator(
      'numeroExterior',
      '',
      'alphanumeric',
      'El campo numero exterior es requerido'
    ),
    'codigoPostal'  : new Validator(
      'codigoPostal',
      '',
      'postalCode',
      'El campo codigo postal es requerido'
    ),
    'estado'        : new Validator(
      'estado',
      '',
      'formatstring',
      'El campo estado es requerido'
    ),
    'municipio'     : new Validator(
      'municipio',
      '',
      'formatstring',
      'El campo municipio es requerido'
    ),
    'colonia'     : new Validator(
      'colonia',
      '',
      'formatstring',
      'El campo colonia es requerido'
    )
  };
  public ruta = 'direccionActiva';
  public config : Config = new Config();

  /**
   *
   * @param router
   * @param route
   * @param direccionesService
   * @param zonificacionService
   * @param location
   * @param validatorService
   * @param globalService
   * @param configService
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private direccionesService: DireccionesService,
    private zonificacionService: ZonificacionService,
    private location: Location,
    private validatorService: ValidatorService,
    private globalService : GlobalService,
    private configService : ConfigService
  ) {
    this.config = this.configService.config
  }

  ngOnInit() {
    this.getDireccion();
    this.setRulesValidate(undefined);
    window.scrollTo(0,0);
  }

  /**
   * Obtiene el objeto direccion por medio del id
   */
  getDireccion(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.direccionesService.getDireccion(id)
      .subscribe((direccion: Direccion) => {
        this.direccion = direccion;
        this.colonias.push(direccion.colonia);
      });
  }
  /**
   * regresa a la locacion anterior
   */
  goBack(): void {
    
    this.router.navigate(['/direcciones']);
  }

  /**
   * Guardar el objeto direccion
   */
  save(): void{
    let validate = this.validateForm(undefined);
    if(validate){
      this.direccionesService.updateDireccion(this.direccion)
        .subscribe( (response : DireccionResponse) => {
          //this.goBack()
          this.envio(response.id)
        });
    }else{
      
    }//end if
  }

  /**
   * Establese una direccion como predeterminada
   * @param id
   */
  envio(id: number): void{
    
    this.direccionesService.setDireccionEnvio(id)
      .subscribe(() => {
        this.router.navigate(['/formas-pago/guardadas']);
        
      });
  }

  /**
   * Obtener zonificacion
   */
  getZoning(cp: string) : void {
    if(cp !== undefined && cp.length >= 5){
      
      this.zonificacionService.getZonificacion(cp)
        .subscribe(zonificacion => this.updateZoning(zonificacion));
    }else{
      this.rules.codigoPostal.error = true
    }//end if
  }

  /**
   * Actualiza los campos de zonificacion
   * @param zonificacion
   */
  updateZoning(zonificacion: Zonificacion): void{
    this.direccion.estado    = zonificacion.estado;
    this.direccion.ciudad    = (zonificacion.ciudad && zonificacion.ciudad.length>1)? zonificacion.ciudad : 'N/A';
    this.direccion.municipio = zonificacion.municipio;
    this.direccion.colonia   = undefined
    this.default             = true
    this.colonias            = zonificacion.colonias;
    this.direccion.codigoEstado = zonificacion.codigoEstado;
    
    if(!zonificacion.estatus){
      this.error   = true
      this.hide    = true
      this.mensaje = zonificacion.error
    } else {
      this.error   = false
      this.mensaje = ''
      this.rules.codigoPostal.error = false
    }
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean{
    let valid: boolean = true;
    let response = this.rules
    this.setRulesValidate(field);
    if( field == undefined){
      response = this.validatorService.validate(this.rules);
    }else{
      let param = {[field] : this.rules[field]}
      response = this.validatorService.validate(param);
    }
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          this.rules[key].error = response[key].error
          valid = false;
        }//end if
      }//end if
    }//end for
    //this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(field : string | undefined ): void{
    if( field !== undefined ){
      this.rules[field].value = this.direccion[field]
    }else{
      this.rules.nombre.value         = this.direccion.nombre;
      this.rules.direccion.value      = this.direccion.direccion;
      this.rules.telefono.value       = this.direccion.telefono;
      this.rules.codigoPostal.value   = this.direccion.codigoPostal;
      this.rules.numeroExterior.value = this.direccion.numeroExterior;
      this.rules.estado.value         = this.direccion.estado;
      this.rules.municipio.value      = this.direccion.municipio;
      this.rules.colonia.value        = this.direccion.colonia;
    }//end if
  }

  /**
   * Router back
   */
  public routerBack(){
      this.router.navigate([this.url]);
  }

}
