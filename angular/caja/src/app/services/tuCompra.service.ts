import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/Rx"
import { of } from "rxjs/observable/of";
import { catchError } from "rxjs/operators";

import { ResumenCompra } from '../entities/resumenCompra';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';


const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class TuCompraService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/detalle-compra'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  public data : ResumenCompra;
  private rcSource = new BehaviorSubject(this.data);

  // Variable que tiene la versión más actualizada del resumen compra
  rcActual = this.rcSource.asObservable();

  /**
   * Obtiene el resumen de la compra
   * @param id_forma
   * @param bim
   * @returns {Observable<ResumenCompra>}
   */
  public resumenCompra(id_forma, bim): Observable<any> {
    let obj =  {idform: id_forma, bim: bim};
    let json     = JSON.stringify(obj);
    let params = "data="+json;

    return this.http.post<ResumenCompra>(this.endPoint, obj, httpSetOptions)
      .pipe(
        catchError(this.handleError('resumenCompra', []))
      );
  }

  /**
   * Setea la información del resumen de la compra para que se puedan suscribir a él.
   * @param {ResumenCompra} resumenCompra
   */

  public setResumenCompra(resumenCompra: ResumenCompra) {
    this.rcSource.next(resumenCompra);
  }

}
