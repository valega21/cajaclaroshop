import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';
import { ClickRecoge } from '../entities/clickRecoge';
import { GlobalService } from '../services/global.service';
import { isNumber, isNullOrUndefined } from 'util';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class ClickRecogeService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string ;

  /**
   * Initialized
   */
  private initialized : boolean;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService : ConfigService,
    private globalService : GlobalService,
    private cookieService : CookieService
  ){ 
    this.config = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/click-recoge'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      window.location.href = this.config.baseUrl
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  initClickRecoge (clickRecoge : ClickRecoge ): Observable<ClickRecoge> {
    localStorage.removeItem(this.configService.config.codeName);
    localStorage.removeItem(this.configService.config.tokenName);
    localStorage.removeItem(this.configService.config.crName);
    let token = this.cookieService.get(this.config.cookieLogin)
    clickRecoge.token = token
    const url = `${this.endPoint}`;
    return this.http.post<ClickRecoge>(url, clickRecoge, httpSetOptions).pipe(
      tap((response: ClickRecoge ) => this.workflow(response)),
      catchError(this.handleError<ClickRecoge>('initClickRecoge'))
    );
  }

  /**
   * 
   * @param response 
   */
  private workflow(response: ClickRecoge ): void{
    if(response.error === true){
      switch(response.action){
        case 1:
          let path = window.location.pathname
          let params = path.split('/')
          let url = params.join('-')
          window.location.href = this.config.baseUrl + 'login?cr=' + url
          break;
      }//end switch
    }else{
      let cr : string[] = [
        response.cliente.toString(), 
        response.producto.toString(), 
        response.sucursal.toString(),
        response.skuHijo.toString()
      ]
      if( isNumber(response.skuHijo) &&  !isNullOrUndefined(response.skuHijo)){
        cr.push(response.skuHijo.toString())
      }//end if
      let url = cr.join('/');
      this.globalService.setCr(url)
      this.globalService.setInitialized(true)
    }//end if
  }

  /**
   * Init click recoge app
   * @param token
   */
  public initClickRecogeApp (token : string ): Observable<ClickRecoge> {
    localStorage.removeItem(this.configService.config.codeName);
    localStorage.removeItem(this.configService.config.tokenName);
    localStorage.removeItem(this.configService.config.crName);
    const url = `${this.endPoint}/${encodeURIComponent(token)}`;
    return this.http.get<ClickRecoge>(url, httpGetOptions).pipe(
        tap((response: ClickRecoge ) => this.workflowApp(response)),
        catchError(this.handleError<ClickRecoge>('initClickRecogeApp'))
    );
  }

  /**
   * workflowApp
   * @param response
   */
  private workflowApp(response: ClickRecoge ): void{
    console.log('pedro....',response);
    if(response.error === true){
      this.globalService.setError(true)
      this.globalService.setMessage(response.message)
    }else{
      let cr : string[] = [
        response.cliente.toString(),
        response.producto.toString(),
        response.sucursal.toString(),
        response.skuHijo.toString()
      ]
      if( isNumber(response.skuHijo) &&  !isNullOrUndefined(response.skuHijo)){
        cr.push(response.skuHijo.toString())
      }//end if
      let url = cr.join('/');
      this.globalService.setCr(url)
      this.globalService.setInitialized(true)
      this.globalService.setIsAppCS(true)
    }//end if
  }

}
