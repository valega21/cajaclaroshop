import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class CuponService {

    /**
     * Config object
     */
    private config : Config = new Config();

    /**
     * End point rest service
     */
    private endPoint : string ;

    constructor(
      private http: HttpClient,
      private configService: ConfigService
    ) {
        this.config   = this.configService.config
        this.endPoint = this.config.apiEndPoint + '/cupon'
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     *  get Cupon
     */
    getCupon(): Observable<any> {
        const url = `${this.endPoint}`;
        return this.http.get<any>(url, httpGetOptions).pipe(
            catchError(this.handleError<any>(`getCupon`))
        );
    }

    /**
     *  validacion de cupon
     */
    validarCupon(id: string): Observable<any> {
        const url = `${this.endPoint}/${id}`;
        return this.http.get<any>(url, httpGetOptions).pipe(
            catchError(this.handleError<any>(`cupon id=${id}`))
        );
    }

    /**
     * eliminacion de cupon antes de consumir
     */
    eliminaCupon(): Observable<any> {
        const url = `${this.endPoint}`;
        return this.http.get<any>(url, httpGetOptions).pipe(
            catchError(this.handleError<any>(`error al eliminar cupon`))
        );
    }

    /**
     * Eliminar cupon
     * @param cupon
     */
    deleteCupon (cupon :string): Observable<any> {
        const url = `${this.endPoint}/${cupon}`;
        return this.http.delete<any>(url, httpGetOptions).pipe(
            catchError(this.handleError<any>('deleteCupon'))
        );
    }

}