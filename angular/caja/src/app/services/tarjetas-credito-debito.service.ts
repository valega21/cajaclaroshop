import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { Tarjeta } from '../entities/tarjeta';
import { TarjetaForm } from '../entities/tarjetaForm';
import { TarjetaResponse } from '../entities/tarjetaResponse';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class TarjetasCreditoDebitoService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   *
   * @param {HttpClient} http
   * @param {ConfigService} configService
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/tarjetas'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtetiene las tarjetas registradas
   */
  getTarjetas(): Observable<Tarjeta[]> {
    return this.http.get<Tarjeta[]>(this.endPoint, httpGetOptions)
      .pipe(
        catchError(this.handleError('getTarjetas', []))
      );
  }


  /**
   *  Obtiene una tarjeta por medio del id
   */
  getTarjeta(id: number): Observable<Tarjeta> {
    const url = `${this.endPoint}/${id}`;
    return this.http.get<Tarjeta>(url, httpGetOptions).pipe(
      catchError(this.handleError<Tarjeta>(`getTarjeta id=${id}`))
    );
  }

  /**
   * Actualizar tarjeta
   * @param direccion
   */
  updateTarjeta  (tarjeta: TarjetaForm): Observable<any> {
    const url = `${this.endPoint}/${tarjeta.id}`;
    return this.http.put(url, tarjeta, httpSetOptions).pipe(
      catchError(this.handleError<TarjetaResponse>('updateTarjeta'))
    );
  }

  /**
   * Agregar una nueva tarjeta|
   * @param tarjeta
   */
  addTarjeta (tarjeta: TarjetaForm): Observable<any> {
    const url = `${this.endPoint}`;
    return this.http.post<TarjetaResponse>(url, tarjeta, httpSetOptions).pipe(
      catchError(this.handleError<TarjetaResponse>('addTarjeta'))
    );
  }

  /**
   * Eliminar tarjeta
   * @param tarjeta
   */
  deleteTarjeta (tarjeta: Tarjeta | number): Observable<any> {
    const id = typeof tarjeta === 'number' ? tarjeta : tarjeta.id;
    const url = `${this.endPoint}/${id}`;
    return this.http.delete<any>(url, httpGetOptions).pipe(
      catchError(this.handleError<any>('deleteTarjeta'))
    );
  }

  /**
   * Define una tarjeta como predeterminada
   * @param id
   */
  defaultTarjeta (id : number, idFormaPago : number): Observable<any> {
    const url = this.endPoint + '/estatus';
    const data = JSON.stringify({ 'id': id, 'action': 1, 'idFormaPago': idFormaPago });
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('defaultTarjeta'))
    );
  }

  /**
   * Define una tarjeta como predeterminada
   * @param id
   */
  setTarjetaPago (id : number, idFormaPago : number): Observable<any> {
    console.log(idFormaPago)
    const url = this.endPoint + '/estatus';
    const data = JSON.stringify({ 'id': id, 'action': 2, 'idFormaPago': idFormaPago });
    console.log(data);
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('setTarjetaPago'))
    );
  }

}
