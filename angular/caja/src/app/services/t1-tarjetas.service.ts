import {Injectable} from '@angular/core';
import {Config} from "../entities/config";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {catchError} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ConfigService} from "./config.service";
import {T1Response} from "../entities/t1Response";
import {T1TarjetaUpdate} from "../entities/T1TarjetaUpdate";
import {T1TarjetaCreate} from "../entities/T1TarjetaCreate";
import {Tarjeta} from "../entities/tarjeta";
import {retryWhen} from "rxjs/operator/retryWhen";
import {interval} from "rxjs/observable/interval";
import {flatMap} from "tslint/lib/utils";
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

const httpGetOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};

@Injectable()
export class T1TarjetasService {

  /**
   * Config object
   */
  private config: Config = new Config();

  /**
   * End point rest service
   */
  private endPoint: string;
  /**
   * Token t1
   */
  private token : string;

  private httpHeaders = {};

  private cajaApi;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.config   = this.configService.config;
    this.endPoint = this.config.t1pagosEndpoint + '/tarjeta';
    //this.endPoint = 'https://api.caja.local.com/caja/api/mocks';
    this.token    = this.config.t1pagosToken;
    this.cajaApi  = this.config.apiEndPoint;

    console.log(this.cajaApi)

    this.httpHeaders = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization' : 'Bearer ' + this.token
      })
    };


  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      if(error.status >= 500 ){
        console.error(error); // log to console instead
      } else if(error.status == 409){
        console.log('sincronizado forzado');
        this.sincronizarTarjetas().subscribe((response ) => {
          console.log(response);
        });

      }
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Consulta al servicio de T1Pagos la tarjeta por token
   * @param {number} token
   * @returns {Observable<T1Response>}
   */
  getTarjeta(token: string): Observable<T1Response> {
    const url = `${this.endPoint}/${token}`;
    return this.http.get<T1Response>(url, this.httpHeaders).pipe(
      catchError(this.handleError<any>(`getTarjeta id=${token}`))
    );
  }

  /**
   * * Actualiza en el servicio de T1Pagos la tarjeta por token
   * @param {string} id
   * @param {T1TarjetaUpdate} tarjeta
   * @returns {Observable<T1Response>}
   */
  updateTarjeta(id: string, tarjeta: T1TarjetaUpdate): Observable<T1Response> {
    const url = `${this.endPoint}/${id}`;
    return this.http.put(url, tarjeta, this.httpHeaders).pipe(
      catchError(this.handleError<any>('updateTarjeta'))
    );
  }

  /**
   * Crear la tarjeta en t1 pagos
   * @param {T1TarjetaCreate} tarjeta
   * @returns {Observable<T1Response>}
   */
  addTarjeta(tarjeta: T1TarjetaCreate): Observable<T1Response> {
    const url = `${this.endPoint}`;
    return this.http.post<T1Response>(url, tarjeta, this.httpHeaders)
      .pipe(
      catchError(this.handleError<any>('addTarjeta'))
    )
  }

  /**
   * Borrar Tarjeta en t1 pagos
   * @param {string} id
   * @returns {Observable<T1Response>}
   */
  deleteTarjeta(id: string): Observable<T1Response> {
    const url = `${this.endPoint}/${id}`;
    return this.http.delete<T1Response>(url, this.httpHeaders).pipe(
      catchError(this.handleError<any>('deleteTarjeta'))
    );
  }

  /**
   * Manda a sincronizar las tarjetas de t1 pagos con la base de datos de api caja
   * @returns {Observable<any>}
   */
  sincronizarTarjetas(): Observable<any> {
    const url = this.cajaApi + '/tarjetas?autorizacion=1';
    return this.http.get<any>(url, httpGetOptions);
  }
}
