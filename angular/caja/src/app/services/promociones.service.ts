import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Promocion} from "../entities/promocion";
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })  
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class PromocionesService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/promociones'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtetiene las promociones registradas
   */
  getPromociones(): Observable<any> {
    return this.http.get<any>(this.endPoint, httpGetOptions)
      .pipe(
        catchError(this.handleError('getPromociones', []))
      );
  }

  /**
   * Agregar una nueva promocion
   * @param direccion
   */
  addPromocion (promo: Promocion): Observable<any> {
    const url = `${this.endPoint}`;
    const data = JSON.stringify(promo);
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('addMensualidad'))
    );
  }

}
