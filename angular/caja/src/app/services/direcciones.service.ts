import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { Direccion } from '../entities/direccion';
import { DireccionResponse } from '../entities/direccionResponse';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class DireccionesService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/direcciones'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtetiene las direcciones registradas
   */
  getDirecciones (): Observable<Direccion[]> {
    return this.http.get<Direccion[]>(this.endPoint, httpGetOptions)
      .pipe(
        catchError(this.handleError('getDirecciones', []))
      );
  }
  
  /**
   *  Obtiene la direccion por medio del id
   */
  getDireccion(id: number): Observable<Direccion> {
    const url = `${this.endPoint}/${id}`;
    return this.http.get<Direccion>(url, httpGetOptions).pipe(
      catchError(this.handleError<Direccion>(`getDireccion id=${id}`))
    );
  }

  /**
   * Actualizar direccion
   * @param direccion 
   */
  updateDireccion (direccion: Direccion): Observable<any> {
    const url = `${this.endPoint}/${direccion.id}`;
    return this.http.put(url, direccion, httpSetOptions).pipe(
      catchError(this.handleError<any>('updateDireccion'))
    );
  }

  /**
   * Agregar una nueva direccion
   * @param direccion
   */
  addDireccion (direccion: Direccion): Observable<any> {
    const url = `${this.endPoint}`;
    return this.http.post<DireccionResponse>(url, direccion, httpSetOptions).pipe(
      catchError(this.handleError<DireccionResponse>('addDireccion'))
    );
  }

  /**
   * Eliminar direccion
   * @param direccion
   */
  deleteDireccion (direccion: Direccion | number): Observable<any> {
    const id = typeof direccion === 'number' ? direccion : direccion.id;
    const url = `${this.endPoint}/${id}`;
    return this.http.delete<any>(url, httpGetOptions).pipe(
      catchError(this.handleError<any>('deleteDireccion'))
    );
  }

  /**
   * Define una direccion como predeterminada
   * @param direccion
   */
  defaultDireccion (id : number): Observable<any> {
    const url = this.config.apiEndPoint + '/direccion/predeterminada';
    const data = JSON.stringify({ 'id': id });
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('defaultDireccion'))
    );
  }

  /**
   * Define una direccion de envio
   * @param direccion
   */
  setDireccionEnvio (id : number): Observable<any> {
    const url = this.config.apiEndPoint + '/direccion/envio';
    const data = JSON.stringify({ 'id': id });
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('setDireccionEnvio'))
    );
  }

  /**
   * Define una direccion de facturacion
   * @param direccion
   */
  setDireccionFacturacion (direccion : Direccion): Observable<any> {
    const url = this.config.apiEndPoint + '/direccion/facturacion';
    direccion.id = 0;
    return this.http.post<any>(url, direccion, httpSetOptions).pipe(
      catchError(this.handleError<any>('setDireccionFacturacion'))
    );
  }
}
