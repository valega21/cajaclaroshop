import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';
import { Carrito } from "../entities/carrito";
import { ClickRecoge } from '../entities/clickRecoge';
import { GlobalService } from '../services/global.service';
import { isNumber, isNullOrUndefined } from 'util';
import {InitResponse} from "../entities/initResponse";

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class CarritoService {

  /**
   * Config object
   */
  private config : Config = new Config();
  /**
   * End point rest service
   */
  private endPoint : string ;

  /**
   * Constructor Carrito Service
   * @param {HttpClient} http
   * @param {CookieService} cookieService
   * @param {ConfigService} configService
   */
  constructor(private http: HttpClient,
              private cookieService : CookieService,
              private configService : ConfigService,
              ) {
    this.config   = this.configService.config;
    this.endPoint = this.config.carritoEndPoint + '/carrito'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      window.location.href = this.config.baseUrl;
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Agrega un producto al carrito
   * @param {Carrito} carrito
   * @returns {Observable<Carrito>}
   */
  public addToCart(carrito : Carrito): Observable<Carrito>{
    // Obtiene el token del carrito desde la cookie
    let token = this.cookieService.get(this.config.cookieName);
    carrito.token = token;
    const url = `${this.endPoint}`;
    return this.http.post<Carrito>(url, Carrito, httpSetOptions).pipe(
      tap((response: Carrito ) => {
        console.log(response)
        return response
      }),
      catchError(this.handleError<any>('addToCart'))
    );
  }

  /**
   * Obtiene un carrito
   * @param {Carrito} carrito
   * @returns {Observable<Carrito>}
   */
  public getCart(carrito : Carrito): Observable<any>{
    const url = `${this.endPoint}/view?id=${carrito.idCarrito}`;
    return this.http.get<any>(url, httpGetOptions).pipe(
      tap((response: any) => {
          console.log('entre a get Cart')
          console.log(response)
      }),
      catchError(this.handleError<any>(`getCart id=${carrito.idCarrito}`))
    );
  }

  public deleteProduct(){

  }

  public getProducts(){

  }


}
