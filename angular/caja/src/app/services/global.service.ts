import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Config } from '../entities/config';
import { ConfigService } from '../services/config.service'

@Injectable()

export class GlobalService {

  private codeSource         = new BehaviorSubject(200);
  private actionSource       = new BehaviorSubject(1);
  private errorSource        = new BehaviorSubject(false);
  private messageSource      = new BehaviorSubject('');
  private loadingSource      = new BehaviorSubject(false);
  private typeMessageSource  = new BehaviorSubject(1);
  private initializedSource  = new BehaviorSubject(false);
  private cuponSource        = new BehaviorSubject('');
  private cvvSource          = new BehaviorSubject('');
  private crSource           = new BehaviorSubject('');
  private loadingIndexSource = new BehaviorSubject(0);
  private direccionesSource  = new BehaviorSubject(false);
  private tdcErrorSource     = new BehaviorSubject(false);
  private authenticationErrorSource = new BehaviorSubject(false);
  private appCSSource = new BehaviorSubject(false);
  private config             = new Config();
  private t1pagosErrorSource = new BehaviorSubject(false);

  public code        = this.codeSource.asObservable();
  public action      = this.actionSource.asObservable();
  public error       = this.errorSource.asObservable();
  public message     = this.messageSource.asObservable();
  public loading     = this.loadingSource.asObservable();
  public typeMessage = this.typeMessageSource.asObservable();
  public initialized = this.initializedSource.asObservable();
  public cupon       = this.cuponSource.asObservable();
  public cvv         = this.cvvSource.asObservable();
  public cr          = this.crSource.asObservable();
  public direcciones = this.direccionesSource.asObservable();
  public tdcError    = this.tdcErrorSource.asObservable();
  public authenticationError = this.authenticationErrorSource.asObservable();
  public isAppCS     = this.appCSSource.asObservable();
  public t1pagosError= this.t1pagosErrorSource.asObservable();

  /**
   * Constructor
   */
  constructor( private configService : ConfigService) {
  }

  /**
   * @param code
   */
  public setCode(code : number ) {
    this.codeSource.next(code);
  }

  /**
   * @param action
   */
  public setAction(action : number ) {
    this.actionSource.next(action);
  }

  /**
   * @param error
   */
  public setError(error : boolean ) {
    this.errorSource.next(error);
  }

  /**
   * @param message
   */
  public setMessage(message : string ) {
    this.messageSource.next(message);
  }

  /**
   * @param loading
   */
  public setLoading(loading : boolean ) {
    this.loadingSource.next(loading);
  }

  /**
   * @param type
   */
  public setTypeMessage(type : number ) {
    this.typeMessageSource.next(type);
  }

  /**
   * @param initialized
   */
  public setInitialized(initialized : boolean ) {
    this.initializedSource.next(initialized);
  }

  /**
   * @param message
   */
  public setCupon(cupon : string ) {
    this.cuponSource.next(cupon);
  }

  /**
   * @param type
   */
  public setCvv( cvv : string ) {
    this.cvvSource.next(cvv);
    localStorage.setItem(this.configService.config.codeName, btoa(cvv))
  }

  /**
   * Get cvv variable
   */
  public getCvv(): void{
    let cvv = localStorage.getItem(this.configService.config.codeName)
    if (cvv != null && cvv != 'null' ){
      this.cvvSource.next(atob(cvv))
    }
  }

  /**
   * Delete cr variable
   */
  public deleteCvv(){
    localStorage.removeItem(this.configService.config.codeName);
  }

  /**
   * @param type
   */
  public setCr( cr : string ) {
    this.crSource.next(cr);
    localStorage.setItem(this.configService.config.crName, btoa( cr ))
  }

  /**
   * Get cvv variable
   */
  public getCr(): void{
    let cr = localStorage.getItem(this.configService.config.crName)
    if (cr != null && cr != 'null' ){
      this.cvvSource.next(atob(cr))
    }
  }

  /**
   * Delete cvv variable
   */
  public deleteCr(){
    localStorage.removeItem(this.configService.config.crName);
  }

  /**
   * @param direcciones
   */
  public setDirecciones(direcciones : boolean ) {
    this.direccionesSource.next(direcciones);
  }

  /**
   * @param error
   */
  public setTdcError(error : boolean ) {
    this.tdcErrorSource.next(error);
  }

  /**
   * @param error
   */
  public setAuthenticationError(error : boolean ) {
    this.authenticationErrorSource.next(error);
  }

  public setIsAppCS(err:boolean) {
    this.appCSSource.next(err);
  }

  public setT1PagosError(error : boolean) {
    this.t1pagosErrorSource.next(error);
  }

}
