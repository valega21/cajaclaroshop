import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

export class ErrorService {
  private errorSource = new BehaviorSubject('');
  errorActual = this.errorSource.asObservable();
  constructor() { }

  setError(error : any) {
    this.errorSource.next(error);
  }

}
