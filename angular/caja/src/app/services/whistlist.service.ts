/*import { Injectable } from '@angular/core';

@Injectable()
export class WhistlistService {

  constructor() { }

  public addWishList(){
    let id;
    id = $(this).attr('data-id');
    if ($(this).hasClass("active")) {
      this.wishlistButton(id);
      return $(this).removeClass("active");
    } else {
      $(this).addClass("active");
      return this.wishlistHome(id);
    }
  }

  public wishlistButton(id) {
    let corazon;
    id = $(this).attr('data-id');
    corazon = $(this);
    if ($(this).hasClass("active")) {
      $.fancybox.open({
        'href': '#modal-wishlistYaAgregado',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
      });
      return $('#eliminarWL').on('click', function() {
        $('#eliminarWL').unbind('click');
        this.wishListeliminar(id);
        $.fancybox.close();
        return corazon.removeClass("active");
      });
    } else {
      $(this).addClass("active");
      return this.wishlistHome(id);
    }
  }


public verifyProducts (){
  $.ajax({
    type: "GET",
    url: '/mi-cuenta/listadeseos/getIdsProductos',
    dataType: 'json',
    success: function(response) {
      return $('.wishlistButton').each(function() {
        return $.each(response, function(key, id) {
          let prod;
          prod = '[data-id="' + id + '"]';
          return $(prod).addClass('active');
        });
      });
    },
    complete: function() {
      return loading(false);
    },
    error: function(result) {
      return loading(false);
    }
  });
  loading(true);
};

public wishlistHome(id) {
  loading(true);
  $.ajax({
    type: "GET",
    url: '/producto/getProd/' + id,
    dataType: 'json',
    success: function(response) {
      let foto, htmlImgs;
      foto = response.fotos[0];
      if (response.tienda["id"] === "3496") {
        foto = foto.replace("/120/", "/1200/");
      }
      foto = proxyImagenes(foto);
      htmlImgs = '<img src = "' + foto + '">';
      $('#vistaRcarouselWL').append(htmlImgs);
      $("#idWL").val(response.id);
      $("#idWL").val(response.id);
      sendWL();
      return $.fancybox.open({
        'href': '#modal-wishlist',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
      });
    },
    beforeSend: function() {
      return $("#vistaRcarouselWL").empty();
    },
    complete: function() {
      return loading(false);
    },
    error: function(result) {
      return loading(false);
    }
  });
  loading(true);
};

public sendWL() {
  let cantidad, color, id, talla;
  color = $("#colorWL").val();
  talla = $("#tallaWL").val();
  id = $("#idWL").val();
  cantidad = 1;
  addProducto(id, cantidad, color, talla);
};

public addProducto(product_id, cantidad, talla, color) {
  $.ajax({
    url: "/mi-cuenta/listadeseos/agrega/",
    type: "post",
    dataType: "json",
    cache: false,
    data: {
      "cantidad": 1,
      "id": product_id
    },
    beforeSend: function() {
      return loading(true);
    },
    success: function(respuesta) {
      let i, ids, index, j, ref, storedNames, tmpIndex;
      if (respuesta.data.login === true) {
        $("#wishlist_count").text(respuesta.data.total_productos);
        $("#idsProducto").val($("#idsProducto").val() + ',' + product_id);
        $("#idsProductos").val($("#idsProductos").val() + ',' + product_id);
      } else {
        ids = [];
        storedNames = JSON.parse(localStorage.getItem("idsProductos"));
        if (storedNames !== void 0 && storedNames !== null) {
          for (i = j = 0, ref = storedNames.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
            tmpIndex = ids.indexOf(storedNames[i]);
            if (!(tmpIndex === -1)) {
              ids.splice(tmpIndex, 1);
            }
            ids.push(storedNames[i]);
          }
        }
        index = ids.indexOf(product_id);
        if (!(index === -1)) {
          ids.splice(index, 1);
        }
        ids.push(product_id);
        localStorage.setItem("idsProductos", JSON.stringify(ids));
        $("#idsProducto").val(ids);
        $("#idsProductos").val(ids);
        $("#wishlist_count").text(ids.length);
      }
      return loading(false);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      loading(false);
      return console.log('Ocurrió un error al agregar el producto a tu lista de deseos.');
    }
  });
};

public wishListeliminar(id, irCarrito) {
  if (irCarrito == null) {
    irCarrito = 0;
  }
  console.log('Prepara para borrar lista de deseos...');
  if (parseInt(document.getElementById('logeado').value) === 1) {
    console.log('Borrando de lista de deseos');
    $.ajax({
      url: '/mi-cuenta/listadeseos/quitar/',
      type: 'post',
      dataType: 'json',
      cache: false,
      data: {
        'id': id
      },
      success: function(respuesta) {
        console.log(respuesta);
        console.log(respuesta.data.total_productos);
        if (respuesta.data.login === true) {
          $('#wishlist_count').text(respuesta.data.total_productos);
          if (irCarrito === 1) {
            document.location = '/carrito/';
          }
        }
        $('#wishlist_producto_' + id).hide();
        loading(false);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        loading(false);
        console.log('Ocurrió un error al quitar de tu lista de deseos.');
      },
      beforeSend: function() {
        loading(true);
      }
    });
  }
};

}
*/
