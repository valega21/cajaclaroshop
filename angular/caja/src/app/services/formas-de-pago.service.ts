import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { FormaPago } from '../entities/formaPago';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';


const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
 
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }),

};

@Injectable()
export class FormasDePagoService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/formas-de-pago'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtetiene las direcciones registradas
   */
  getFormasDePago (): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.endPoint, httpGetOptions)
      .pipe(
        catchError(this.handleError('getFormasDePago', []))
      );
  }

  /**
   * Definir la forma de pago seleccionada
   * @param direccion
   */
  setFormaDePago (id: number): Observable<any> {
    const url = `${this.endPoint}`;
    const data = JSON.stringify({ 'idFormaDePago': id });
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('setFormaPago'))
    );
  }

}
