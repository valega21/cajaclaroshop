import { Injectable } from '@angular/core';

@Injectable()
export class DeviceFingerPrintService {

  constructor() { }

  public cybs_dfprofiler(merchantID: string, environment:string): number {
    if (environment.toLowerCase() === 'live') {
        var org_id = 'k8vif92e'
    } else {
        var org_id = '1snn5n9w'
    }
    let sessionID = new Date().getTime();
    let pTM = document.createElement("p") as HTMLParagraphElement;
    let str = "background:url(https://h.online-metrix.net/fp/clear.png?org_id=" + org_id + "&session_id=" + merchantID + sessionID + "&m=1)";
    //pTMstyleSheet = str;
    //pTM.height  = "1";
    //pTM.width = "1";
    pTM.hidden = true
    document.body.appendChild(pTM);
    var img = document.createElement("img");
    str = "https://h.online-metrix.net/fp/clear.png?org_id=" + org_id + "&session_id=" + merchantID + sessionID + "&m=2";
    img.src = str;
    document.body.appendChild(img);
    var tmscript = document.createElement("script");
    tmscript.src = "https://h.online-metrix.net/fp/check.js?org_id=" + org_id + "&session_id=" + merchantID + sessionID;
    tmscript.type = "text/javascript";
    document.body.appendChild(tmscript);
    var objectTM = document.createElement("object");
    objectTM.data = "https://h.online-metrix.net/fp/fp.swf?org_id=" + org_id + "&session_id=" + merchantID + sessionID;
    objectTM.width = "1";
    objectTM.height = "1";
    objectTM.id = "thm_fp";
    var param = document.createElement("param");
    param.name = "movie";
    param.value = "https://h.online-metrix.net/fp/fp.swf?org_id=" + org_id + "&session_id=" + merchantID + sessionID;
    objectTM.appendChild(param);
    document.body.appendChild(objectTM);
    
    return sessionID
  }
}
