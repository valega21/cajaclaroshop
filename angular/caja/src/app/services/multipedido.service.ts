import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { Multipedido } from '../entities/multipedido';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }) 
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class MultipedidoService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/multipedido'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      //window.location.href = this.config.baseUrl + 'caja'

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtetiene las direcciones registradas
   */
  getMultipedido (): Observable<Multipedido> {
    return this.http.get<Multipedido>(this.endPoint, httpGetOptions)
      .pipe(
        catchError(this.handleError<Multipedido>('getMultipedido'))
      );
  }

  /**
   * Se puede setear el dato en forma de pago
   * @param id
   */
  setDataFormaPago (formaPago : any): Observable<any> {
    const url = this.endPoint + '/multipedido';
    const data = JSON.stringify({ 'formaPago': formaPago, 'action': 1 });
    return this.http.post<any>(url, data, httpSetOptions).pipe(
        catchError(this.handleError<any>('setTarjetaPago'))
    );
  }

}
