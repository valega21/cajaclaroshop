import { TestBed, inject } from '@angular/core/testing';

import { PaymentMonederoService } from './payment-monedero.service';

describe('PaymentMonederoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaymentMonederoService]
    });
  });

  it('should be created', inject([PaymentMonederoService], (service: PaymentMonederoService) => {
    expect(service).toBeTruthy();
  }));
});
