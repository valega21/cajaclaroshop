import { TestBed, inject } from '@angular/core/testing';

import { PaymentT1PagosService } from './payment-t1-pagos.service';

describe('PaymentT1PagosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaymentT1PagosService]
    });
  });

  it('should be created', inject([PaymentT1PagosService], (service: PaymentT1PagosService) => {
    expect(service).toBeTruthy();
  }));
});
