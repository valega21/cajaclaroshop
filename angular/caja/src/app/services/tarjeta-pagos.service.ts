import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConfigService} from "./config.service";
import {Config} from "../entities/config";
import {T1TarjetaUpdate} from "../entities/T1TarjetaUpdate";
import {of} from "rxjs/observable/of";
import {catchError} from "rxjs/operators";
import {T1Response} from "../entities/t1Response";
import {T1TarjetaCreate} from "../entities/T1TarjetaCreate";
import {Tarjeta} from "../entities/tarjeta";

const httpSetOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const httpGetOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};


@Injectable()
export class TarjetaPagosService {

  /**
   * Config object
   */
  private config: Config = new Config();

  /**
   * End point rest service
   */
  private endPoint: string;

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.config = this.configService.config;
    this.endPoint = this.config.apiEndPoint + '/tarjetaPagos';
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * * Actualiza en el servicio de T1Pagos la tarjeta por token
   * @param {string} id
   * @param {T1TarjetaUpdate} tarjeta
   * @returns {Observable<T1Response>}
   */
  updateTarjeta(id: any, tarjeta: T1TarjetaUpdate): Observable<any> {
    const url = `${this.endPoint}/${id}`;
    return this.http.put(url, tarjeta, httpSetOptions).pipe(
      catchError(this.handleError<any>('updateTarjeta'))
    );
  }

  /**
   * Crear la tarjeta en t1 pagos
   * @param {T1TarjetaCreate} tarjeta
   * @returns {Observable<T1Response>}
   */
  addTarjeta(tarjeta: T1TarjetaCreate): Observable<any> {
    const url = `${this.endPoint}`;
    return this.http.post<T1Response>(url, tarjeta, httpSetOptions).pipe(
      catchError(this.handleError<any>('addTarjeta'))
    );
  }

  /**
   * Borrar Tarjeta en t1 pagos
   * @param {string} id
   * @returns {Observable<T1Response>}
   */
  deleteTarjeta(id: string): Observable<T1Response> {
    const url = `${this.endPoint}/${id}`;
    return this.http.delete<T1Response>(url, httpGetOptions).pipe(
      catchError(this.handleError<T1Response>('deleteTarjeta'))
    );
  }

  /**
   *  Obtiene una tarjeta por medio del id
   */
  getTarjeta(id: number): Observable<Tarjeta> {
    const url = `${this.endPoint}/${id}`;
    return this.http.get<Tarjeta>(url, httpGetOptions).pipe(
      catchError(this.handleError<Tarjeta>(`getTarjeta id=${id}`))
    );
  }

}
