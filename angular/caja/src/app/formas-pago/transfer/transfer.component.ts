import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Validator } from '../../entities/validator';
import { ValidatorService } from '../../services/validator.service';
import { MultipedidoService } from '../../services/multipedido.service';
import { ErrorService } from "../../services/error.service";

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.sass']
})
export class TransferComponent implements OnInit {

  @Input() codigo : string = '';

  public title    : string = 'Ingresa tu código Transfer';
  public error    : string ;
  public loading  : boolean = false;
  public url      : string  = '/formas-pago/guardadas'

  public rules = {
    'codigo'   : new Validator(
      'codigo', 
      '', 
      'number',
      'El campo codigo es requerido'
    ),
  };

  constructor(
      private validatorService : ValidatorService,
      private multipedidoService : MultipedidoService,
      private router: Router,
      private errorService : ErrorService
  ){}

  ngOnInit(){
    this.errorService.errorActual.subscribe( error => {
      this.error = error;
    });
    window.scrollTo(0,0);
  }

  /**
   * Save code
   */
  public save(): void{
    this.loading = true;
    let validate = this.validateForm();
    if(validate){
      this.multipedidoService.setDataFormaPago(this.codigo)
        .subscribe( (response ) => {
          this.goConfirmacion()
        });
    }else{
      this.loading = false;
    }//end if
  }

  /**
   * Redirecciona confirmacion
   */
  goConfirmacion(): void {
    this.router.navigate(['/confirmacion']);
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(): boolean{
    let valid: boolean = true;
    this.setRulesValidate();
    let response = this.validatorService.validate(this.rules);
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          valid = false;
        }//end if
      }//end if
    }//end for
    this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(): void{
    this.rules.codigo.value = this.codigo;
  }
}
