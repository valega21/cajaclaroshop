import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Tarjeta} from '../../../entities/tarjeta';
import {TarjetasCreditoDebitoService} from '../../../services/tarjetas-credito-debito.service';
import {DatalayerService} from "../../../services/datalayer.service";
import {GlobalService} from '../../../services/global.service';
import {Subscription} from 'rxjs/Subscription';
import {T1TarjetasService} from "../../../services/t1-tarjetas.service";
import {T1TarjetaUpdate} from "../../../entities/T1TarjetaUpdate";
import {TarjetaPagosService} from "../../../services/tarjeta-pagos.service";
import {ConfigService} from "../../../services/config.service";

@Component({
  selector: "app-tg",
  templateUrl: "./tarjetas-guardadas.component.html",
  styleUrls: ['./tarjetas-guardadas.component.sass']
})


export class tarjetasGuardadasComponent implements OnInit {

  public idT            : number;
  public title          : string = "Elige tu Forma de Pago";
  public loading        : boolean = true;
  public breadcrumb     : boolean = true;
  public tarjetas       : Tarjeta[];
  public ruta           : string = 'pagosActiva';
  public url            : string = '/direcciones';
  public crSubscription : Subscription;
  public idFormaPago    : number;
  public idFpT1pagos    : number;

  /**
   *
   * @param {Router} router
   * @param {TarjetasCreditoDebitoService} tarjetasService
   * @param {DatalayerService} datalayerService
   * @param {GlobalService} globalService
   * @param {T1TarjetasService} t1TarjetaService
   * @param {TarjetaPagosService} tarjetaPagosService
   * @param {ConfigService} configService
   */
  constructor(private router: Router,
              private tarjetasService: TarjetasCreditoDebitoService,
              private datalayerService: DatalayerService,
              private globalService: GlobalService,
              private t1TarjetaService: T1TarjetasService,
              private tarjetaPagosService : TarjetaPagosService,
              private configService : ConfigService)
  {
    this.idFpT1pagos = this.configService.getConfig().t1pagosId;
  }

  ngOnInit() {
    this.globalService.setCvv('')
    this.crSubscription = this.globalService.cr
      .subscribe(cr => {
        if (cr && cr.length > 0) {
          this.url = '/click-recoge/' + cr
        }//end if
      })
    this.getTarjetas()
  }

  /**
   * Obtiene las tarjetas almacenadas por el usuario
   */
  private getTarjetas(): void {
    this.tarjetasService.getTarjetas()
      .subscribe(tarjetas => {
        console.log(tarjetas)
        this.tarjetas = tarjetas
        this.prepareTarjetas(tarjetas);
      });
  }

  /**
   * Evalua el numero de tarjetas registradas y redirecciona
   */
  private setWorkFlow(): void {
    if (this.tarjetas.length <= 0) {
      this.router.navigate(['/formas-pago']);
    }//end if
    
  }

  /**
   * Elimina una tarjeta
   * @param id
   */
  delete(tarjeta: Tarjeta): void {
    if (tarjeta.idFormaPago === this.idFpT1pagos) {
      this.t1TarjetaService.deleteTarjeta(tarjeta.token)
        .subscribe(() => {
          this.t1TarjetaService.sincronizarTarjetas().subscribe(tarjetas => {
            this.tarjetas = tarjetas;
            this.prepareTarjetas(tarjetas);
            this.globalService.setError(false);
          })
        });
    } else {
      this.tarjetasService.deleteTarjeta(tarjeta.id)
        .subscribe(() => {
          this.getTarjetas()
        });
    }
  }

  /**
   * Establece una tarejeta como predeterminada
   * @param {Tarjeta} tarjeta
   */
  default(tarjeta: Tarjeta): void {
    if (tarjeta.idFormaPago === this.idFpT1pagos) {
        let  tarjetaUpdate = new T1TarjetaUpdate();
        tarjetaUpdate.default = true;
        this.t1TarjetaService.updateTarjeta(tarjeta.token, tarjetaUpdate)
              .subscribe(() => {
                this.t1TarjetaService.sincronizarTarjetas().subscribe(tarjetas => {
                  this.prepareTarjetas(tarjetas);
                });
              });
    } else {
      this.tarjetasService.defaultTarjeta(tarjeta.id, 3912)
        .subscribe(() => {
          this.getTarjetas()
        });
    }
  }

  /**
   * Redirecciona al editar tarjetas t1pagos o stratus
   * @param {Tarjeta} tarjeta
   */
  edit(tarjeta: Tarjeta): void {
    if (tarjeta.idFormaPago === this.idFpT1pagos) {
      this.router.navigate(['/formas-pago/t1pagos/' + tarjeta.token]);
    } else {
      this.router.navigate(['/formas-pago/tarjetas/' + tarjeta.id]);
    }
  }


  /**
   * Establece una tarjeta como forma de pago
   * @param id
   */
  setPago(id: number, idFormaPago : number): void {
    this.tarjetasService.setTarjetaPago(id, idFormaPago)
      .subscribe((tarjeta) => {
        // DataLayer Forma Pago
        this.datalayerService.setCheckoutStep3(tarjeta.idFPago)
        if(tarjeta.idFPago == 4384 /* departamentales */  ||
           tarjeta.idFPago == 4379 /* sanborns */         ||
           tarjeta.idFPago == 4375 /* sears */ ) {
          this.router.navigate(['/formas-pago/departamental/promociones']);
        } else {
          this.router.navigate(['/formas-pago/tarjeta/mensualidades']);
        }
      });
  }

  /**
   *
   * @param digitos
   * @param type
   */
  public formatCard(digitos: string, type: string): string {
    let response: string;
    switch (type) {
      case 'amex':
        response = '**** ****** ' + digitos;
        break;
      case 'mastercard':
        response = '**** **** **** ' + digitos;
        break;
      case 'visa':
        response = '**** **** **** ' + digitos;
        break;
      case 'sears':
        response = '** *****' + digitos;
        break;
      case 'sanborns':
        response = '** *****' + digitos;
        break;
      default:
        response = '**** **** **** ' + digitos;
        break;
    }
    return response
  }

  /**
   *
   * @param digitos
   * @param type
   */
  public imageCard(type: string): string {
    let response: string;
    switch (type) {
      case 'amex':
        response = './assets/logo-amex1.svg';
        break;
      case 'mastercard':
        response = './assets/logo-mastercard1.svg';
        break;
      case 'visa':
        response = './assets/logo-visa1.svg';
        break;
      case 'sears':
        response = './assets/logo-t-sears1.png';
        break;
      case 'sanborns':
        response = './assets/logo-t-sanborns1.png';
        break;
      default:
        response = './assets/logo-visa1.svg';
        break;
    }
    return response
  }

  /**
   * Merge formas de pago
   */
  private orderTarjetas() {
    this.tarjetas.sort(function (a: Tarjeta, b: Tarjeta) {
      return +b.predet - +a.predet;
    });
  }

  /**
   * Set ID Pago
   * @param id
   */
  public setIdPago(id: number, idFormaPago): void {
    this.idT = id
    this.idFormaPago = idFormaPago;
    console.log(this.idFormaPago)
  }

  /**
   *
   */
  private setIdtPredet(): void {
    this.idT = this.tarjetas[0].id;
    this.idFormaPago = this.tarjetas[0].idFormaPago;
  }

  private prepareTarjetas(tarjetas : Tarjeta[]) {
    this.tarjetas = tarjetas;
    this.setWorkFlow();
    this.orderTarjetas();
    this.setIdtPredet();
  }
}
