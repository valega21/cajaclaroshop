import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormaPago } from '../entities/formaPago';
import { FormasDePagoService } from '../services/formas-de-pago.service';
import { ErrorService } from "../services/error.service";
import { DatalayerService } from "../services/datalayer.service";
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';
import {ConfigService} from "../services/config.service";

@Component({
  selector:"app-formasPago",
  templateUrl: "./formas-pago.component.html",
  styleUrls: ['./formas-pago.component.sass']
})

export class formasPagoComponent {

  public title          : string = 'Selecciona una Forma de Pago';
  public pendientePago  : string = 'Lo sentimos, por el momento no puedes realizar tu compra con cargo a tu recibo Telmex.'
  public pendientePago2 : string = ' Elige otra forma de pago';
  public loading        : boolean= true;
  public error          : any;
  public ruta           : string = 'pagosActiva';
  public hide           : boolean= true;
  public url            : string = '/direcciones';
  public crSubscription : Subscription;
  public tdcError       : boolean= true;
  public tdcErrorSubscription : Subscription;
  public idFpT1pagos    : number;

  /**
   *
   * @param route
   * @param router
   * @param formasDePagoService
   * @param errorService
   * @param datalayerService
   * @param globalService
   * @param configService
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formasDePagoService: FormasDePagoService,
    private errorService : ErrorService,
    private datalayerService: DatalayerService,
    private globalService : GlobalService,
    private configService : ConfigService
  ){
    this.idFpT1pagos = this.configService.getConfig().t1pagosId;

    this.assets[3158] = { url: '/formas-pago/telmex',          img: './assets/logo-telmex.svg',                   name : 'Recibo Telmex'};
    this.assets[3912] = { url: '/formas-pago/tarjetas',        img: './assets/logo-visa.svg',                     name : 'Tarjetas de Crédito y Débito'};
    this.assets[4450] = { url: '/formas-pago/tiendas',         img: './assets/logo-sears-sanborns.svg',           name : 'Tiendas departamentales'};
    this.assets[1389] = { url: '/formas-pago/conveniencia',    img: './assets/logo-oxxo-seven.svg',               name : 'Tiendas de conveniencia'};
    this.assets[5]    = { url: '/confirmacion',                img: './assets/logo-paypal.svg',                   name : 'PayPal'};
    this.assets[4384] = { url: '/formas-pago/departamentales', img: './assets/tc-sears-sanborns.png',             name : 'Tarjetas departamentales'};
    this.assets[4]    = { url: '/formas-pago/transferencia',   img: './assets/logo-bancos.svg',                   name : 'Depósito Bancario'};
    this.assets[4448] = { url: '/formas-pago/transfer',        img: './assets/logo-transfer.svg',                 name : 'Transfer'};
    this.assets[4466] = { url: '/formas-pago/monedero',        img: './assets/monedero.png',                      name : 'Monedero Claro shop'};
    this.assets[this.idFpT1pagos]
                      = { url: '/formas-pago/t1pagos',         img: './assets/logo-visa.svg',                     name : 'Tarjetas de Crédito y Débito'};

    this.order[3158] = { weight: 0};
    this.order[3912] = { weight: 1};
    this.order[this.idFpT1pagos]
                     = { weight: 2};
    this.order[4450] = { weight: 3};
    this.order[1389] = { weight: 4};
    this.order[5]    = { weight: 5};
    this.order[4384] = { weight: 6};
    this.order[4]    = { weight: 7};
    this.order[4466] = { weight: 8};
  }

  /**
   * Array Objects Direccion
   */
  public formasDePago: FormaPago[];

  /**
   * Assets
   */
  public assets = {};
  public order  = {};

  /**
   * Hook init()
   */
  ngOnInit() {
    let error = this.route.snapshot.params.error != undefined ? this.route.snapshot.params.error : undefined;
    if(error != undefined){
      this.errorService.setError('Lo sentimos, algo salió mal. Elige otra forma de pago')
    }
    this.crSubscription = this.globalService.cr
    .subscribe( cr =>{
      if(cr && cr.length > 0){
        this.url = '/click-recoge/' + cr
      }//end if
    })

    this.tdcErrorSubscription = this.globalService.tdcError
    .subscribe( error =>{
      this.tdcError = error
    })
    this.getFormasDePago();
    this.errorService.errorActual.subscribe( error => {
      this.error = error
      this.hide  = true
    });
    window.scrollTo(0,0);
  }

  ngOnDestroy(){
    this.errorService.setError('')
    this.globalService.setTdcError(false)
  }

  /**
   * Obtiene las formas de pago por medio del servicio formas de pago
   */
  private getFormasDePago(): void {

    this.formasDePagoService.getFormasDePago()
      .subscribe(formasDePago => {
        this.formasDePago = formasDePago
        console.log(this.formasDePago)
        this.orderFormasDePago()

      });

  }

  /**
   * Establese la forma de pago seleccionada
   * @param id
   */
  public setFormaDePago(id: number): void{

    this.formasDePagoService.setFormaDePago(id)
      .subscribe(() => {
        // DataLayer Forma Pago
        this.datalayerService.setCheckoutStep3(id)
        this.routeFormaPago(id)

      });
  }

  /**
   * Route fomas de pago
   * @param id
   */
  public routeFormaPago(id: number): void{

    if (this.assets.hasOwnProperty(id)){
      this.router.navigate([this.assets[id].url]);
    }//end if

  }

  /**
   * Merge formas de pago
   */
  private orderFormasDePago(){
    for(let key in this.formasDePago){
      if(this.order[this.formasDePago[key].idFormadePago] !== undefined){
        this.formasDePago[key].order = this.order[this.formasDePago[key].idFormadePago].weight
      }//end if
    }//end for
    this.formasDePago.sort(function(a : FormaPago,b : FormaPago){
      return a.order - b.order;
    });
  }
}
