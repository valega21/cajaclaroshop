import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TarjetasCreditoDebitoService} from "../../services/tarjetas-credito-debito.service";
import {TarjetaForm} from "../../entities/tarjetaForm";
import {Expiry} from "../../entities/expiry";
import {ErrorService} from "../../services/error.service";
import {GlobalService} from "../../services/global.service";
import {Subscription} from "rxjs/Rx";
import {ActivatedRoute, Router} from "@angular/router";
import {DatalayerService} from "../../services/datalayer.service";
import {CardValidatorService} from "../../services/card-validator.service";
import {T1TarjetasService} from "../../services/t1-tarjetas.service";
import {T1Response} from "../../entities/t1Response";
import {T1Tarjeta} from "../../entities/t1Tarjeta";
import {isNumber, isString} from "util";
import {T1TarjetaUpdate} from "../../entities/T1TarjetaUpdate";
import {T1TarjetaCreate} from "../../entities/T1TarjetaCreate";
import {DireccionesService} from "../../services/direcciones.service";
import {MultipedidoService} from "../../services/multipedido.service";
import {ZonificacionService} from "../../services/zonificacion.service";
import {ValidatorService} from "../../services/validator.service";
import {Direccion} from "../../entities/direccion";
import {Multipedido} from "../../entities/multipedido";
import {Validator} from "../../entities/validator";
import {Zonificacion} from "../../entities/zonificacion";
import {T1Direccion} from "../../entities/T1Direccion";
import {T1ClientesService} from "../../services/t1-clientes.service";
import {TarjetaPagosService} from "../../services/tarjeta-pagos.service";
import {ConfigService} from "../../services/config.service";
import {T1Telefono} from "../../entities/t1Telefono";

@Component({
  selector: 'app-t1pagos',
  templateUrl: './t1pagos.component.html',
  styleUrls: ['./t1pagos.component.sass']
})
export class T1pagosComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() tarjeta : T1Tarjeta = new T1Tarjeta();
  @Input() expiry  : Expiry    = new Expiry();

  public tarjetaForm : TarjetaForm = new TarjetaForm();
  public title       : string  = "Agregar Nueva Forma de Pago";
  public loading     : boolean = true;
  public breadcrumb  : boolean = true;
  public disabled    : boolean = false;
  public id          : string;
  public show        : boolean = false;
  public cvv         : string;
  public anios       : any     = [];
  public error       : any;
  public ruta        : string  = 'pagosActiva';
  public cvvHelp     : string  = 'El cvv se encuentra en la parte posterior de la tarjeta'
  public isEdit      : boolean = false;
  public cvvSubscription: Subscription;
  public hide        : boolean = true;
  public rules = {
    valid: true,
    nombre: { error : false},
    numero: { error : false},
    vencimiento: { error : false},
    codigo: { error : false}
  };
  public meses: any = [
    {mes: "01", name: "01 - Enero"},
    {mes: "02", name: "02 - Febrero"},
    {mes: "03", name: "03 - Marzo"},
    {mes: "04", name: "04 - Abril"},
    {mes: "05", name: "05 - Mayo"},
    {mes: "06", name: "06 - Junio"},
    {mes: "07", name: "07 - Julio"},
    {mes: "08", name: "08 - Agosto"},
    {mes: "09", name: "09 - Septiembre"},
    {mes: "10", name: "10 - Octubre"},
    {mes: "11", name: "11 - Noviembre"},
    {mes: "12", name: "12 - Diciembre"}
  ];
  public t1direccion     : T1Direccion = new T1Direccion();
  @Input() direccionForm : Direccion   = new Direccion();
  public facturacion = "Dirección del estado de cuenta";
  public isDireccionFacturacion = true;
  public envio      : string    = 'Envio';
  public entrega    : string    = 'Entrega';
  public mensualidad: number    = 1;
  public multipedido: Multipedido;
  public direccion  : Direccion = new Direccion();
  public colonias   : String[]  = [];
  public showCode   : boolean   = false;
  public showEdit   : boolean   = false;
  public idFormaPago: number;
  public infoCargada: boolean   = false;
  public nombrePromo: string;
  public clickRecoge: boolean   = false;
  public isLoadDireccion: boolean = false;
  public isLoadTarjeta  : boolean = false;
  public mensaje    : string    = '';
  public default    : boolean   = true;
  private conditions: string[]  = ['idCliente', 'idDireccion'];
  public errorT1pagos;
  public direccionEnvio;
  public idFpT1pagos : number;
  public t1Telefono = new T1Telefono();
  public direccionMultipedido : Direccion = new Direccion();
  public rulesDireccion = {
    'direccion': new Validator(
      'direccion',
      '',
      'alphanumeric',
      'El campo direccion es requerido'
    ),
    'numeroExterior': new Validator(
      'numeroExterior',
      '',
      'number',
      'El campo numero exterior es requerido'
    ),
    'codigoPostal': new Validator(
      'codigoPostal',
      '',
      'postalCode',
      'El campo codigo postal es requerido'
    ),
    'estado': new Validator(
      'estado',
      '',
      'formatstring',
      'El campo estado es requerido'
    ),
    'municipio': new Validator(
      'municipio',
      '',
      'formatstring',
      'El campo municipio es requerido'
    ),
    'colonia': new Validator(
      'colonia',
      '',
      'formatstring',
      'El campo colonia es requerido'
    )
  };

  /**
   *
   * @param {Router} router
   * @param {ActivatedRoute} route
   * @param {TarjetasCreditoDebitoService} tarjetasService
   * @param {CardValidatorService} cardValidatorService
   * @param {ErrorService} errorService
   * @param {GlobalService} globalService
   * @param {DatalayerService} datalayerService
   * @param {T1TarjetasService} t1TarjetaService
   * @param {T1ClientesService} t1ClienteService
   * @param {DireccionesService} direccionesService
   * @param {MultipedidoService} multipedidoService
   * @param {ZonificacionService} zonificacionService
   * @param {ValidatorService} validatorService
   * @param {TarjetaPagosService} tarjetaPagosService
   * @param {ConfigService} configService
   */
  constructor(private router            : Router,
              private route             : ActivatedRoute,
              private tarjetasService   : TarjetasCreditoDebitoService,
              private cardValidatorService: CardValidatorService,
              private errorService      : ErrorService,
              private globalService     : GlobalService,
              private datalayerService  : DatalayerService,
              private t1TarjetaService  : T1TarjetasService,
              private t1ClienteService  : T1ClientesService,
              private direccionesService: DireccionesService,
              private multipedidoService: MultipedidoService,
              private zonificacionService: ZonificacionService,
              private validatorService  : ValidatorService,
              private tarjetaPagosService: TarjetaPagosService,
              private configService     : ConfigService
  )
  {
    this.idFpT1pagos = this.configService.getConfig().t1pagosId;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.getYears();
    this.getTarjeta();
    this.cvvSubscription = this.globalService.cvv
      .subscribe(cvv => {
        this.cvv = cvv
      });

    this.errorService.errorActual.subscribe(error => {
      this.error = error
    });
    this.globalService.t1pagosError.subscribe( error => {
      this.errorT1pagos = error;
    });
    this.globalService.message.subscribe( error => {
      if ( this.error == '' || this.error == undefined) {
        this.error = error;
      }
    });

    console.log(this.error);
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.globalService.setT1PagosError(false);
    this.errorService.setError('');
    this.globalService.setError(false);
  }

  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
    this.getMultipedido()
  }

  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido(): void {
    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
        this.multipedido = multipedido;
        this.validateConditions();
        this.mensualidad = multipedido.mensualidades;
        this.clickRecoge = multipedido.clickRecoge;
        this.idFormaPago = this.multipedido.idFormaDePago;
        this.getDireccion(multipedido.idDireccion);
        this.nombrePromo = this.multipedido.nombrePromocion;
      });
  }

  /**
   * Actualiza el valor de la variable expiry (mes y año de expiración de la tarjeta)
   */
  public updateExpiry() {
    let anio = this.expiry.anio == undefined ? '••' : this.expiry.anio;
    let mes  = this.expiry.mes  == undefined ? '••' : this.expiry.mes;
    this.expiry.expiry = mes + '/' + anio
    this.tarjetaForm.vencimiento = this.expiry.expiry;
    if ((anio != undefined && anio != '••') && (mes != undefined && mes != '••')) {
      this.validateForm('vencimiento')
    }//end if
    setTimeout(function () {
      let event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true);
      let tarjeta: HTMLElement = document.querySelector('input[id="expiry"]') as HTMLElement;
      tarjeta.dispatchEvent(event);
    }, 50)
  }

  /**
   * Obtiene el objeto tarjeta por medio del id desde la url
   */
  getTarjeta(): void {
    let id  = this.route.snapshot.paramMap.get('id');
    this.id = id;
    if (isString(id)) {
      this.isEdit = true
      this.t1TarjetaService.getTarjeta(id)
        .subscribe((response: T1Response) => {
          this.disabled = true;
          this.setObjectTarjetaForm(response.data.tarjeta);
          this.setObjectDireccion(response.data.tarjeta.direccion);
          this.initCardFormat()
        });
    } else {
      this.show = false;
      this.showEdit = false;
      this.facturacion = "Dirección de Envío";
      this.isDireccionFacturacion = false;
    }
  }

  /**
   * Obtiene el objeto direccion por medio del id
   * @param {number} id
   */
  private getDireccion(id: number): void {
      this.direccionesService.getDireccion(id)
        .subscribe((direccion: Direccion) => {
          this.direccionMultipedido = direccion;
          if (!isString(this.route.snapshot.paramMap.get('id')) && !this.clickRecoge) {
            this.direccion = direccion;
            this.t1Telefono.codigo_pais = 52;
            this.t1Telefono.numero = +this.direccion.telefono;
            console.log(this.direccion)
            this.isLoadDireccion = true
          } else if(this.multipedido.idDireccion == 0 && Object.keys(this.direccion).length === 0 ) {
            this.show = true;
          }
        });
  }

  /**
   * Cambia la dirección del formulario
   * @param $event
   */
  changeDireccion($event) {
    this.show = $event;
    this.direccionForm = this.direccion;
    this.getZoning(this.direccion.codigoPostal, false)
  }

  /**
   * Regresa a las tarjetas guardadas
   */
  goListTarjetas(): void {
    this.router.navigate(['/formas-pago/t1pagos/guardadas']);
  }

  /**
   * Guardar el objeto tarjeta y lo envía a t1 pagos y setea la forma de pago en el multipedido
   */
  save(): void {
    let validate = this.validateForm(undefined);
    let validateDireccion = this.validateFormDireccion(undefined);
    if (validate && validateDireccion) {
      this.globalService.setCvv(this.tarjetaForm.codigo);
      this.saveDireccion();
      if (this.isEdit) {
        let tarjetaUpdate = this.setT1CardUpdateObject();
        console.log(tarjetaUpdate);
        // update en t1 pagos
        this.t1TarjetaService.updateTarjeta(this.tarjeta.token, tarjetaUpdate)
          .subscribe((responseT1: T1Response) => {
            if(responseT1.status === 'success'){
              this.t1TarjetaService.sincronizarTarjetas().subscribe(response => {
                response.forEach(tarjeta => {
                  if(tarjeta.token === this.route.snapshot.paramMap.get('id')){
                    this.setPago(tarjeta.id);
                  }
                })
              });
            }
          });
      } else {
        let tarjetaCreate = this.setT1CardCreateObject();
        // get cliente
        this.t1ClienteService.addCliente().subscribe((cliente: any) => {
          tarjetaCreate.cliente_id = cliente.data.clienteT1;
          // create en t1pagos
          this.t1TarjetaService.addTarjeta(tarjetaCreate)
            .subscribe((tarjeta: T1Response) => {
              if(tarjeta != undefined){
                // create en caja api
                this.tarjetaPagosService.addTarjeta(tarjeta.data.tarjeta).subscribe(response => {
                  this.setPago(response.id);
                });
              }
            }
          ); // End addTarjeta t1
        }); // End addCliente
      }
    }
  }

  /**
   * Crea el objeto para crear la tarjeta para t1 pagos
   * @returns {T1TarjetaCreate}
   */
  public setT1CardCreateObject(): T1TarjetaCreate {
    let tarjetaCreate             = new T1TarjetaCreate();
    tarjetaCreate.nombre          = this.tarjetaForm.nombre;
    tarjetaCreate.pan             = parseInt(this.tarjetaForm.numero.replace(/[^0-9*]/g, ''));
    tarjetaCreate.cvv2            = parseInt(this.tarjetaForm.codigo);
    tarjetaCreate.expiracion_mes  = parseInt(this.expiry.mes);
    tarjetaCreate.expiracion_anio = parseInt(this.expiry.anio);
    tarjetaCreate.direccion       = this.t1direccion;
    tarjetaCreate.default         = true;
    tarjetaCreate.cargo_unico     = false;
    tarjetaCreate.direccion.pais  = 'MEX';
    return tarjetaCreate;
  }

  /**
   * Crea el objeto tarjeta para el update en t1 pagos
   * @returns {T1TarjetaUpdate}
   */
  public setT1CardUpdateObject(): T1TarjetaUpdate {
    let tarjetaUpdate             = new T1TarjetaUpdate();
    tarjetaUpdate.nombre          = this.tarjetaForm.nombre;
    tarjetaUpdate.expiracion_mes  = parseInt(this.expiry.mes);
    tarjetaUpdate.expiracion_anio = parseInt(this.expiry.anio);
    tarjetaUpdate.default         = true;
    tarjetaUpdate.direccion       = this.t1direccion;
    tarjetaUpdate.direccion.estado= this.direccionForm.codigoEstado;
    tarjetaUpdate.direccion.pais  = 'MEX';
    return tarjetaUpdate;
  }

  /**
   * Establece una tarjeta como forma de pago
   * @param id
   */
  setPago(id: number): void {
    this.tarjetasService.setTarjetaPago(id, this.idFpT1pagos)
      .subscribe((resultado) => {
        this.router.navigate(['/formas-pago/tarjeta/mensualidades']);
    })
  }

  /**
   * Define los parametros requeridos para el formulario tarjeta
   * @param tarjeta
   */
  setObjectTarjetaForm(tarjeta: T1Tarjeta): void {
    tarjeta.pan                 = tarjeta.pan.slice(0, 6);
    this.tarjeta                = tarjeta;
    this.expiry.mes             = ('0' + tarjeta.expiracion_mes.toString()).slice(-2)
    this.expiry.anio            = (tarjeta.expiracion_anio.toString()).slice(-2)
    this.expiry.expiry          = this.expiry.mes + '/' + this.expiry.anio;
    this.tarjetaForm.id         = tarjeta.token;
    this.tarjetaForm.nombre     = tarjeta.nombre;
    this.tarjetaForm.numero     = this.formatCard(tarjeta.pan, tarjeta.marca);
    this.tarjetaForm.vencimiento= this.expiry.expiry;
    this.tarjetaForm.codigo     = this.formatCode(this.tarjeta.marca);
    this.tarjetaForm.tipo       = this.tarjeta.marca;
  }

  /**
   * Crea el objeto dirección a partir de una direccion de t1 pagos
   * @param {T1Direccion} direccion
   */
  setObjectDireccion(direccion : T1Direccion):void {
    this.direccion.direccion      = direccion.linea1;
    this.direccion.calle          = direccion.linea1;
    let arrLinea2                 = direccion.linea2.split(',');
    this.direccion.numeroExterior = arrLinea2[0].trim();
    this.direccion.numeroInterior = arrLinea2[1].trim();
    this.direccion.colonia        = arrLinea2[2].trim();
    this.direccion.codigoPostal   = direccion.cp;
    this.direccion.estado         = direccion.estado;
    this.direccion.municipio      = direccion.municipio;
    this.direccion.ciudad         = direccion.ciudad.length > 0 ? direccion.ciudad : 'N/A';
    let t1Telefono = new T1Telefono();
    if(direccion.hasOwnProperty('telefono')) {
      t1Telefono.numero =  direccion.telefono.numero;
      t1Telefono.codigo_pais = direccion.telefono.codigo_pais;
      this.direccion.telefono = t1Telefono.numero.toString() != undefined ? t1Telefono.numero.toString() : '';
    }
    this.isLoadDireccion          = true;
    this.isDireccionFacturacion   = true;
  }

  /**
   * @param digitos
   * @param type
   */
  public formatCard(digitos: string, type: string): string {

    let response: string = '';
    let gaps: number[] = [4, 8, 12];
    let card: string = '';
    console.log(type);

    switch (type) {
      case 'amex':
        card = digitos + '*********';
        gaps = [4, 10];
        break;
      case 'mastercard':
      case 'visa':
        card = digitos + '**********';
        gaps = [4, 8, 12];
        break;
      default:
        card = digitos + '**********';
        gaps = [4, 8, 12];
        break;
    }
    for (var i = 0; i < card.length; i++) {
      for (let gap of gaps) {
        if (i == gap && i > 0) {
          response = response + ' ';
        }//end if
      }//end for
      response = response + card[i];
    }//end for
    return response
  }

  /**
   * @param digitos
   * @param type
   */
  public formatCode(type: string): string {
    let response: string;
    switch (type) {
      case 'amex':
        response = '****';
        break;
      default:
        response = '***';
        break;
    }
    return response
  }

  /**
   *
   */
  public initCardFormat(): void {
    setTimeout(function () {
      let event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true);
      let number: HTMLElement = document.querySelector('input[name="number"]') as HTMLElement;
      let name: HTMLElement = document.querySelector('input[name="name"]') as HTMLElement;
      let expiry: HTMLElement = document.querySelector('input[name="expiry"]') as HTMLElement;
      let code: HTMLElement = document.querySelector('input[name="cvc"]') as HTMLElement;
      number.dispatchEvent(event);
      name.dispatchEvent(event);
      expiry.dispatchEvent(event);
    }, 500);
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field: string | undefined): boolean {
    let response = this.cardValidatorService.validateCardForm(this.tarjetaForm);
    this.cvvHelp = response.tipo == 'amex' ? 'En caso de ser amex se muestra en la parte frontal de la tarjeta' : 'El cvv se encuentra en la parte posterior de la tarjeta';
    this.tarjetaForm.tipo = response.tipo;
    if (field !== undefined) {
      this.rules[field].error = response[field].error
    } else {
      this.rules = response;
    }
    return response.valid;
  }

  /**
   * Get years
   */
  private getYears(): any {
    let year = new Date().getFullYear();
    for (var entry = year; entry < year + 8; entry++) {
      let text = entry.toString()
      let anio = text.substring(text.length - 2);
      this.anios.push({anio: anio, name: text})
    }//end for
  }

  /**
   * Paso 4 para el chekout
   * @param $event
   */
  public setStep4($event) {
    this.datalayerService.setCheckoutStep4();
  }

  /**
   * Guardado de la dirección de la tarjeta en el multipedido
   */
  public saveDireccion() {
    if (this.show === true) {
      let validate = this.validateFormDireccion(undefined);
      if (validate) {
        this.direccion = this.direccionForm;
      }
    }
    this.t1direccion.ciudad    = this.direccion.ciudad;
    this.t1direccion.cp        = this.direccion.codigoPostal;
    this.t1direccion.estado    = this.direccion.codigoEstado;
    this.t1direccion.municipio = this.direccion.municipio;
    this.t1direccion.pais      = 'MEX';
    this.t1direccion.linea1    = this.direccion.direccion;
    this.t1direccion.linea2    = this.direccion.numeroExterior
      + ', ' + ( (this.direccion.numeroInterior == undefined || this.direccion.numeroInterior == "") ? '' : this.direccion.numeroInterior.replace(/,/g , ' ').trim())
      + ', ' + this.direccion.colonia;
    this.direccion.ciudad      = this.direccion.ciudad ? this.direccion.ciudad : 'N/A';
    this.t1direccion.ciudad    = this.direccion.ciudad ? this.direccion.ciudad : 'N/A';
    let t1Telefono = new T1Telefono();
    t1Telefono.codigo_pais     = 52;
    if(this.direccion.telefono == undefined){
      this.direccion.telefono  = this.direccionMultipedido.telefono
    }
    t1Telefono.numero          = parseInt(this.direccion.telefono);
    this.t1direccion.telefono  = t1Telefono;
    this.direccionesService.setDireccionFacturacion(this.direccion).subscribe();
  }

  /**
   * Obtener zonificacion
   * @param {string} cp
   */
  getZoning(cp: string, limpiarForm: boolean): void {
    if (cp !== undefined && cp.length >= 5) {
      this.zonificacionService.getZonificacion(cp)
        .subscribe(zonificacion => {
          this.updateZoning(zonificacion, limpiarForm)
        });
    } else {
      this.rulesDireccion.codigoPostal.error = true
    }//end if
  }

  /**
   * Actualiza los campos de zonificacion
   * @param zonificacion
   */
  updateZoning(zonificacion: Zonificacion, limpiarForm: boolean): void {
    if (!zonificacion.estatus) {
      this.hide  = true
      this.mensaje = zonificacion.error
      return;
    }
    this.error  = false;
    this.mensaje= '';
    this.rulesDireccion.codigoPostal.error = false
    this.direccionForm.estado       = zonificacion.estado;
    if( (zonificacion.ciudad !== null || zonificacion.ciudad.length > 0) && limpiarForm === true) {
      this.direccionForm.ciudad = zonificacion.ciudad;
    } else if(this.direccionForm.ciudad === null)  {
      this.direccionForm.ciudad = 'N/A';
    }
    this.direccionForm.municipio    = zonificacion.municipio;
    this.direccionForm.colonia      = zonificacion.colonias.includes(this.direccionForm.colonia) ? this.direccionForm.colonia : undefined;
    this.direccionForm.codigoEstado = zonificacion.codigoEstado;
    this.default  = true;
    this.colonias = zonificacion.colonias;
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateFormDireccion(field: string | undefined): boolean {
    let valid: boolean = true;
    if(this.show){
      let response = this.rulesDireccion;
      this.setRulesValidate(field);
      if (field == undefined) {
        response = this.validatorService.validate(this.rulesDireccion);
      } else {
        let param = {[field]: this.rulesDireccion[field]};
        response = this.validatorService.validate(param);
      }
      for (let key in response) {
        if (response.hasOwnProperty(key)) {
          if (response[key].error) {
            this.rulesDireccion[key].error = response[key].error;
            valid = false;
          }//end if
        }//end if
      }//end for
      //this.rules = response;
    }
    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   * @param {string | undefined} field
   */
  private setRulesValidate(field: string | undefined): void {
    if (field !== undefined) {
      this.rulesDireccion[field].value = this.direccionForm[field]
    } else {
      this.rulesDireccion.direccion.value      = this.direccionForm.direccion;
      this.rulesDireccion.codigoPostal.value   = this.direccionForm.codigoPostal;
      this.rulesDireccion.numeroExterior.value = this.direccionForm.numeroExterior;
      this.rulesDireccion.estado.value         = this.direccionForm.estado;
      this.rulesDireccion.municipio.value      = this.direccionForm.municipio;
      this.rulesDireccion.colonia.value        = this.direccionForm.colonia;
    }//end if
  }

  /**
   * Redirecciona a confirmación
   */
  goConfirm(): void {
    this.router.navigate(['/confirmacion']);
  }

  /**
   * Valida que se tengan todas las condiciones
   */
  private validateConditions(): void {
    if (this.multipedido.clickRecoge) {
      return;
    } else {
      switch (this.multipedido.idFormaDePago) {
        case 5:
          this.conditions = ['idCliente', 'idFormaDePago', 'idDireccion'];
          break;
        default:
          break;
      }//end switch
    }//end if
    for (let key of this.conditions) {
      if (typeof this.multipedido[key] == 'undefined' || !this.multipedido[key]) {
        this.router.navigate(['/direcciones']);
      }//end if
    }//end if
  }

}
