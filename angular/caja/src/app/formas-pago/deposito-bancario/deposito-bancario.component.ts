import { Component, OnInit } from '@angular/core';
import { DepositoService } from "../../services/deposito.service";
import { Router } from "@angular/router";
import { GlobalService } from '../../services/global.service';

@Component({
  selector:     'app-deposito-bancario',
  templateUrl:  './deposito-bancario.component.html',
  styleUrls: [  './deposito-bancario.component.sass' ]
})

export class DepositoBancarioComponent implements OnInit {

  private banco   : string = "inbursa";
  private datos   : any = { banco: this.banco };
  public title    : string = 'Selecciona un Banco';
  public id_forma : number = 4;
  public loading  : boolean = false;
  public ruta     : string = 'pagosActiva';
  public default  : string = "inbursa";
  public bancos   : any = [
    { nombre: "inbursa" ,       img: "./assets/logo-inbursa.svg"  },
    { nombre: "bancomer",       img: "./assets/logo-bancomer.svg" },
    { nombre: "santander",      img: "./assets/logo-santander.svg"},
    { nombre: "banamex" ,       img: "./assets/logo-banamex.svg"  },
  ];

  /**
   * 
   * @param depositoService 
   * @param router 
   * @param globalService 
   */
  constructor(
    private depositoService: DepositoService,
    private router: Router,
    private globalService : GlobalService
  ) { }

  ngOnInit() {
  }

  /**
   * Define el banco
   * @param $event 
   * @param value 
   */
  public selectBanco($event, value) {
    
    $event.preventDefault();
    this.banco  = value;
    this.datos  = { banco: this.banco };
    this.setBanco(this.datos).then((banco) =>{
      this.router.navigate(['/confirmacion']);
      

    });
  }

  /**
   * Genera el request al servicio banco
   * @param datos 
   */
  public setBanco(datos){
    return this.depositoService.setBanco(datos).toPromise();
  }
}
