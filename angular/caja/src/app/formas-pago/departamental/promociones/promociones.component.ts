import { Component, OnInit } from '@angular/core';
import { Mensualidad } from "../../../entities/mensualidad";
import { Promocion } from "../../../entities/promocion";
import { PromocionesService } from "../../../services/promociones.service";
import { Router } from "@angular/router";
import { DatalayerService } from "../../../services/datalayer.service";

@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',

})
export class promocionesComponent implements OnInit {
public title  = "Seleccionar Mensualidades";

public loading        : boolean = true;
public breadcrumb     : boolean = true;
public promociones    : Promocion[];
public mensualidades  : Mensualidad[];
public mensualidad    : Promocion = new Promocion();
public promocion      : Promocion[];
public titlePromocion : string;
public checked        : boolean;
public idFormaPago    : number;
public promo          : Promocion;
public nombreFormaPago: string;
public ruta           = 'pagosActiva';

  /**
   *
   * @param {PromocionesService} promocionesService
   * @param {Router} router
   * @param {DatalayerService} datalayerService
   */
  constructor(
    private promocionesService : PromocionesService,
    private router: Router,
    private datalayerService : DatalayerService
  ) {
}

  ngOnInit() {
    window.scrollTo(0,0);
    this.getPromociones();
  }

  /**
   * Obtiene las mensualidades disponibles para el pedido
   */
  private getPromociones():void {

    this.promocionesService.getPromociones()
      .subscribe(resultado => {

        if(resultado.status){
          this.promocion      = resultado.promocion;
          this.titlePromocion = resultado.promocion.nombre;
          this.checked        = resultado.checked;
          this.setPromoPredet()
        } else {
          this.router.navigate(['/confirmacion']);
        }

      });
  }

  /**
   * Guardar el numero de mensualidades
   */
  public save(): void{

    this.promo = this.mensualidad;
    this.promo.pp = this.mensualidad.pp ? this.mensualidad.pp : 0;
    this.promo.sec = this.mensualidad.sec ? this.mensualidad.sec : 0;
    this.promo.meses = this.mensualidad.meses ? this.mensualidad.meses : 0;
    this.promo.mensualidad = this.mensualidad.mensualidad ? this.mensualidad.mensualidad : 0;
    this.promo.nombre = this.mensualidad.nombre ? this.mensualidad.nombre : '';
    this.promo.idFormaPago = this.idFormaPago ? this.idFormaPago : 0;
    this.promocionesService.addPromocion(this.promo)
      .subscribe((response : any) => {
        this.goConfirmacion(response);

      });
  }

  /**
   * Define el id de la mensualidad seleccionada
   * @param mensualidad
   */
  public setMensualidad(mensualidad) : void {
    this.mensualidad = mensualidad;
    // DataLayer Step 4
    this.datalayerService.setCheckoutStep4()
  }

  /**
   * Define la promoción
   * @param mensualidad
   */
  public setIdFormaPago(id: number) : void {
    this.idFormaPago = id;
  }

  /**
   * Define la promoción
   * @param mensualidad
   */
  public setNombreFormaPago(nombre: string) : void {
    this.mensualidad.nombre = nombre;
  }

  /**
   * regresa a la locacion anterior
   */
  private goConfirmacion(response: any): void {
    if(response.estatus == true){
      this.router.navigate(['/confirmacion']);
    }//end if

  }

  /**
   * Define la opcion predeterminada
   */
  private setPromoPredet():void{
    for( let promocion of this.promocion ){
      this.mensualidad             = promocion;
      this.mensualidad.nombre      = promocion.nombre ? promocion.nombre : '';
      this.mensualidad.idFormaPago = promocion.idFormaPago ? promocion.idFormaPago : 0;
      this.idFormaPago             = promocion.idFormaPago ? promocion.idFormaPago : 0;
      if(promocion.datos != undefined ){
        this.mensualidad.pp          = promocion.datos[0].pp ? promocion.datos[0].pp : 0;
        this.mensualidad.sec         = promocion.datos[0].sec ? promocion.datos[0].sec : 0;
        this.mensualidad.meses       = promocion.datos[0].meses ? promocion.datos[0].meses : 0;
        this.mensualidad.mensualidad = promocion.datos[0].mensualidad ? promocion.datos[0].mensualidad : 0;
        promocion.datos[0].active    = true
      }
      promocion.active = true
      break;
    }
  }
}
