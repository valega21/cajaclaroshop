import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Multipedido } from '../../../entities/multipedido';
import { MultipedidoService } from '../../../services/multipedido.service';
import { DireccionTelmex } from '../../../entities/direccionTelmex';
import { Validator }       from '../../../entities/validator';
import { TelmexResponse }  from '../../../entities/telmexResponse';
import { ValidatorService } from '../../../services/validator.service';
import { TelmexService } from '../../../services/telmex.service';
import { GlobalService } from '../../../services/global.service';
import { ErrorService } from '../../../services/error.service';

@Component({
  selector:"app-cuentaTelmex",
  templateUrl: "./cuenta-telmex.component.html",
  styleUrls: ['./cuenta-telmex.component.sass']
})

export class cuentaTelmexComponent implements OnInit{

  @Input() direccion: DireccionTelmex = new DireccionTelmex();
  private conditions: string[] = ['idCliente', 'idFormaDePago', 'idDireccion', 'formaDePago'];
  public multipedido : Multipedido;
  public ruta    : string = 'pagosActiva';
  public title   : string = 'Cuenta Mi Telmex';
  public error   : string ;
  public message : string;
  public loading : boolean = false;
  public hide    : boolean = true;

  public rules = {
    'telefono' : new Validator(
      'telefono',
      '',
      'phone',
      'El campo telefono es requerido'
    ),
    'contrasena'   : new Validator(
      'contrasena',
      '',
      'formatstring',
      'El campo contrasena es requerido'
    ),
  };

  constructor(
    private validatorService : ValidatorService,
    private telmexService    : TelmexService,
    private router: Router,
    private globalService : GlobalService,
    private errorService : ErrorService,
    private multipedidoService: MultipedidoService
  ){}

  ngOnInit(){
    window.scrollTo(0,0);
    this.getMultipedido()
  }

  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido():void {
    
    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
          this.multipedido = multipedido
          this.direccion.telefono = multipedido.formaDePago.toString()
          this.validateConditions()
      });
  } 

  /**
   * login telmex
   */
  public login(): void{
    
    let validate = this.validateForm(undefined);
    if(validate){
      this.telmexService.loginTelmex(this.direccion)
        .subscribe( (response : TelmexResponse) => {
          this.processLogin(response)
        });
    }else{
      
    }//end if
  }

  private processLogin(response: TelmexResponse): void{
    if(response.status == true){
      this.router.navigate(['/formas-pago/telmex/mensualidades']);
    }else{
      this.message = response.error
      
      this.errorService.setError(response.error);
      this.router.navigate(['/formas-pago']);
    }
  }


  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean{
    let valid: boolean = true;
    let response = this.rules
    this.setRulesValidate(field);
    if( field == undefined){
      response = this.validatorService.validate(this.rules);
    }else{
      let param = [this.rules[field]]
      response = this.validatorService.validate(param);
    }
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          this.rules[key].error = response[key].error
          valid = false;
        }//end if
      }//end if
    }//end for
    //this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(field : string | undefined ): void{
    if( field !== undefined ){
      this.rules[field].value = this.direccion[field]
    }else{
      this.rules.telefono.value       = this.direccion.telefono;
      this.rules.contrasena.value     = this.direccion.contrasena;
    }//end if
  }

  /**
   * 
   * @param url 
   */
  public goPassTelmex(url): void{
    window.open(url,'_blank');
  }

  /**
   * validate conditions
   */
  private validateConditions(): void{

    if( this.multipedido.clickRecoge ){
      return;
    }
    for (let key of this.conditions){
      if ( typeof this.multipedido[key] == 'undefined' || !this.multipedido[key]){
        this.router.navigate(['/direcciones']);
      }//end if
    }//end if
  }
}
