import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Multipedido } from '../../../entities/multipedido';
import { MultipedidoService } from '../../../services/multipedido.service';
import { DireccionTelmex } from '../../../entities/direccionTelmex';
import { Validator }       from '../../../entities/validator';
import { TelmexResponse }  from '../../../entities/telmexResponse';
import { Zonificacion }    from '../../../entities/zonificacion';
import { ZonificacionService } from '../../../services/zonificacion.service';
import { ValidatorService } from '../../../services/validator.service';
import { TelmexService } from '../../../services/telmex.service';
import { GlobalService } from '../../../services/global.service';
import { ErrorService } from '../../../services/error.service';

@Component({
  selector    : 'app-sinCuenta',
  templateUrl : './sin-cuenta.component.html',
  styleUrls   : ['./sin-cuenta.component.sass']
})

export class sinCuentaComponent implements OnInit{

  @Input() direccion: DireccionTelmex = new DireccionTelmex();
  private conditions: string[] = ['idCliente', 'idFormaDePago', 'idDireccion', 'formaDePago'];
  public ruta     : string = 'pagosActiva';
  public title    : string = 'Crear cuenta Mi Telmex';
  public message  : string;
  public error    : boolean = false ;
  public colonias : String[] = [];
  public loading  : boolean = false;
  public hide     : boolean = true;
  public default  : boolean = true;
  public multipedido : Multipedido;
  public rules = {
    'nombre' : new Validator(
      'nombre',
      '',
      'string',
      'El campo nombre es requerido'
    ),
    'direccion' : new Validator(
      'direccion',
      '',
      'alphanumeric',
      'El campo direccion es requerido'
    ),
    'telefono' : new Validator(
      'telefono',
      '',
      'phone',
      'El campo telefono es requerido'
    ),
    'numeroExterior': new Validator(
      'numeroExterior',
      '',
      'number',
      'El campo numero exterior es requerido'
    ),
    'codigoPostal'  : new Validator(
      'codigoPostal',
      '',
      'postalCode',
      'El campo codigo postal es requerido'
    ),
    'estado'        : new Validator(
      'estado',
      '',
      'string',
      'El campo estado es requerido'
    ),
    'municipio'     : new Validator(
      'municipio',
      '',
      'formatstring',
      'El campo municipio es requerido'
    ),
    'colonia'     : new Validator(
      'colonia',
      '',
      'formatstring',
      'El campo colonia es requerido'
    )
  };

  constructor(
    private zonificacionService : ZonificacionService,
    private telmexService    : TelmexService,
    private validatorService    : ValidatorService,
    private router: Router,
    private globalService : GlobalService,
    private errorService  : ErrorService,
    private multipedidoService: MultipedidoService,
  ){
  }

  ngOnInit(){
    window.scrollTo(0,0);
    this.getMultipedido()
  }

  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido():void {
    
    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
          this.multipedido = multipedido
          this.direccion.telefono = multipedido.formaDePago.toString()
          this.validateConditions()
      });
  } 

  /**
   * Login form
   */
  public loginForm():void{
    
    let validate = this.validateForm(undefined);
      if(validate){
        this.telmexService.loginFormTelmex(this.direccion)
          .subscribe( (response : TelmexResponse) => {
            this.processLogin(response)
          });
      }else{
        
      }//end if
  }

  /**
   * Process login
   * @param response 
   */
  private processLogin(response: TelmexResponse): void{
    if(response.status == true){
      this.router.navigate(['/formas-pago/telmex/mensualidades']);
    }else{
      this.message = response.error
      
      this.errorService.setError(response.error);
      this.router.navigate(['/formas-pago']);
    }
  }

  /**
   * Obtener zonificacion
   */
  getZoning(cp: string) : void {
    if(cp !== undefined && cp.length >= 5){
      
      this.zonificacionService.getZonificacion(cp)
        .subscribe(zonificacion => {
          this.updateZoning(zonificacion)
          
        });
    }else{
      this.rules.codigoPostal.error = true
    }//end if
  }

  /**
   * Actualiza los campos de zonificacion
   * @param zonificacion
   */
  updateZoning(zonificacion: Zonificacion): void{
    this.direccion.estado    = zonificacion.estado;
    this.direccion.ciudad    = zonificacion.ciudad;
    this.direccion.municipio = zonificacion.municipio;
    this.direccion.colonia   = undefined
    this.default             = true
    this.colonias = zonificacion.colonias;
    if(!zonificacion.estatus){
      this.error   = true
      this.hide    = true;
      this.message = zonificacion.error
    } else {
      this.error   = false
      this.message = ''
      this.rules.codigoPostal.error = false
    }
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean{
    let valid: boolean = true;
    let response = this.rules
    this.setRulesValidate(field);
    if( field == undefined){
      response = this.validatorService.validate(this.rules);
    }else{
      let param = {[field] : this.rules[field]}
      response = this.validatorService.validate(param);
    }//end if
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          this.rules[key].error = response[key].error
          valid = false;
        }//end if
      }//end if
    }//end for
    //this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(field : string | undefined ): void{
    if( field !== undefined ){
      this.rules[field].value = this.direccion[field]
    }else{
      this.rules.nombre.value         = this.direccion.nombre;
      this.rules.direccion.value      = this.direccion.direccion;
      this.rules.telefono.value       = this.direccion.telefono;
      this.rules.codigoPostal.value   = this.direccion.codigoPostal;
      this.rules.numeroExterior.value = this.direccion.numeroExterior;
      this.rules.estado.value         = this.direccion.estado;
      this.rules.municipio.value      = this.direccion.municipio;
      this.rules.colonia.value        = this.direccion.colonia;
    }//end if
  }

  /**
   * validate conditions
   */
  private validateConditions(): void{
    if( this.multipedido.clickRecoge ){
      return;
    }
    for (let key of this.conditions){
      if ( typeof this.multipedido[key] == 'undefined' || !this.multipedido[key]){
        this.router.navigate(['/direcciones']);
      }//end if
    }//end if
  }
}
