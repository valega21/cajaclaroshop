import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DireccionTelmex } from '../../entities/direccionTelmex';
import { Validator }       from '../../entities/validator';
import { TelmexResponse }  from '../../entities/telmexResponse';
import { ValidatorService } from '../../services/validator.service';
import { TelmexService } from '../../services/telmex.service';
import { GlobalService } from '../../services/global.service';
import { ErrorService } from '../../services/error.service';

@Component({
  selector:"app-telmex",
  templateUrl: "./telmex.component.html",
  styleUrls: ['./telmex.component.sass']

})

export class telmexComponent implements OnInit{

  @Input() direccion: DireccionTelmex = new DireccionTelmex();
  public ruta     : string = 'pagosActiva';
  public title    : string = 'Ingresa tus datos Mi Telmex';
  public message  : string = 'Debes contar con una cuenta "Mi Telmex" para esta forma de pago. ¿Aún no la tienes?  <a class="linkE" href="https://mitelmex.telmex.com/web/hogar/registro" target="_blank">click aquí</a>  para crearla.';
  public error    : string ;
  public loading  : boolean = false;
  public hide     : boolean = true;
  public rules = {
    'telefono' : new Validator(
      'telefono',
      '',
      'phone',
      'El campo telefono es requerido'
    )
  };

  /**
   * Constructor
   * @param validatorService 
   * @param telmexService 
   * @param router 
   * @param globalService 
   */
  constructor(
    private validatorService : ValidatorService,
    private telmexService    : TelmexService,
    private router: Router,
    private globalService : GlobalService,
    private errorService : ErrorService
  ){}

  ngOnInit(){
      window.scrollTo(0,0);
  }

  /**
   * Valida tipo de linea telmex
   */
  public validate(): void{
    
    let validate = this.validateForm();
    if(validate){
      this.telmexService.validaTipoLinea(this.direccion)
        .subscribe( (response : TelmexResponse) => {
          this.processLogin(response)
        });
    }else{
      
    }//end if
  }

  /**
   * 
   * @param response 
   */
  private processLogin(response: TelmexResponse): void{
    if(response.status == true){
      switch(response.tipo){
        case 1:
          this.router.navigate(['/formas-pago/telmex/cuenta-telmex']);
          break;
        case 2:
          this.router.navigate(['/formas-pago/telmex/sin-cuenta']);
          break;
      }//end switch
    }else{
      
      switch(response.tipo){
        case 0:
          this.message = response.message
          break;
        case 3:
        case 6:
        case 4:
        case 5:
        case 7:
          this.message = response.message
          this.errorService.setError(response.message);
          this.router.navigate(['/formas-pago']);
          break;
      }//end switch
    }//end if
  }
  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(): boolean{
    let valid: boolean = true;
    this.setRulesValidate();
    let response = this.validatorService.validate(this.rules);
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          valid = false;
        }//end if
      }//end if
    }//end for
    this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(): void{
    this.rules.telefono.value       = this.direccion.telefono;
  }
}
