import { Component } from '@angular/core';
import {tarjetasGuardadasComponent} from "../tc/tarjetas-guardadas/tarjetas-guardadas.component";

@Component({
  selector: 'app-tarjetaRechazada',
  templateUrl: './tarjetaRechazada.component.html',
  styleUrls: ['tarjetaRechazada.component.sass']

})
export class tarjetaRechazadaComponent extends tarjetasGuardadasComponent {

}
