import {Component, Input, OnInit} from '@angular/core';
import {DepositoService} from "../../services/deposito.service";
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-confirmacion-deposito-bancario',
  templateUrl: './deposito-bancario.component.html',
  styleUrls: ['./deposito-bancario.component.sass']
})
export class ConfirmacionDepositoBancarioComponent implements OnInit {

  constructor(
    private DepositoService: DepositoService,
    private globalService : GlobalService
  ) { }
  public loading     : boolean = false;
  public nombreBanco;
  @Input('showEdit') showEdit : boolean = false;

  ngOnInit() {
    this.getBanco().then((banco) =>{
      this.nombreBanco = banco.banco;
      
    });

  }

  public getBanco(){
    return this.DepositoService.getBanco().toPromise();
  }



}
