import { Component, OnInit, Input } from '@angular/core';
import { Checkout } from '../../entities/checkout';

@Component({
  selector: 'app-forma-pago-paypal',
  templateUrl: './forma-pago-paypal.component.html',
  styleUrls: ['./forma-pago-paypal.component.sass']
})
export class FormaPagoPaypalComponent implements OnInit {

  @Input() checkout: Checkout = new Checkout()
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
