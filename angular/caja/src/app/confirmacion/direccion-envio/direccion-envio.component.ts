import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Direccion } from '../../entities/direccion';

@Component({
  selector: 'app-direccion-envio',
  templateUrl: './direccion-envio.component.html',
  styleUrls: ['./direccion-envio.component.sass']
})
export class DireccionEnvioComponent implements OnInit {

  @Input('direccion') direccion : Direccion = new Direccion();
  @Input('showEdit') showEdit : boolean = false;
  @Input('title') title : string = "Dirección de envío";
  @Input('isDireccionFacturacion') isDireccionFacturacion : boolean = false;
  @Output() changeDireccion : EventEmitter <boolean> = new EventEmitter<boolean>();
  public show :boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showFormDireccion(event){
    this.changeDireccion.emit(this.show);
  }

}
