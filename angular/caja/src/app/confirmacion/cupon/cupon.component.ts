import { Component, Input, OnInit } from '@angular/core';
import { CuponService } from '../../services/cupon.service';
import { Cupon } from '../../entities/cupon';
import { GlobalService } from '../../services/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cupon',
  templateUrl: './cupon.component.html',
  styleUrls: ['./cupon.component.sass']
})
export class CuponComponent implements OnInit {

  public loading    : boolean = false;
  public showCupon  : boolean = false;
  public mensaje : string;
  public messageSubscription : Subscription;
  public codigo  : string;
  public codigoSubscription : Subscription;
  public importe : number;
  public hide : boolean = true;

  /**
   * Constructor
   * @param cuponService 
   */
  constructor(
    private cuponService : CuponService,
    private globalService : GlobalService
  ){}


  ngOnInit() {
      this.messageSubscription = this.globalService.message
        .subscribe(message => {this.mensaje = message});
      this.codigoSubscription = this.globalService.cupon
        .subscribe(cupon => {this.codigo = cupon});
      this.getCupon()
  }

  /**
   * Get cupon
   */
  private getCupon(){
    
    this.cuponService.getCupon()
    .subscribe((cupon: Cupon) => {
        this.globalService.setMessage(cupon.message)
        this.globalService.setCupon(cupon.cupon)
        this.mensaje   = cupon.message
        this.codigo    = cupon.cupon
        this.importe   = cupon.importe
        this.showCupon = cupon.status
        
    });
  }

  /**
   * Valida si el cupon existe
   */
  public validar(cupon: string): void{
    
    this.cuponService.validarCupon(cupon)
        .subscribe((cupon: Cupon) => {
          if(cupon.status) {
            this.codigo    = cupon.cupon
            this.globalService.setCupon(cupon.cupon)
            this.importe   = cupon.importe
            this.showCupon = true
          } else {
            this.showCupon = false
            this.hide      = true
            this.mensaje   = cupon.message
            this.globalService.setMessage(cupon.message)
             
          }
          
        });
  }

    /**
     * Eliminar cupon ya validado
     */
    public eliminarCupon(cupon :string): void{
      
      this.cuponService.deleteCupon(cupon)
          .subscribe((cupon: Cupon) => {
            this.showCupon = !cupon.status
            
          })
    }

    /**
     * 
     * @param cupon 
     */
    public setCupon(cupon : string): void{
      this.globalService.setCupon(cupon)
    }


}
