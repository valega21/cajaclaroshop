import {Component, Input, ViewChild, AfterViewInit, Output, EventEmitter} from '@angular/core';
import { Tarjeta } from '../../entities/tarjeta';
import { Checkout } from '../../entities/checkout';
import { Validator } from '../../entities/validator';
import { Captcha } from '../../entities/captcha';
import { Direccion } from '../../entities/direccion';
import { GlobalService } from '../../services/global.service';
import { Subscription } from 'rxjs';
import { isNumber } from 'util';
import {DatalayerService} from "../../services/datalayer.service";

const SEARSREVOLVENTE   = 4375;
const SEARS             = 4384;
const SEARSSININTERESES = 4385;
const SANBORNS          = 4379;
const CREDITO           = 3158;
const DEBITO            = 3912;

@Component({
  selector: 'app-detallePago',
  templateUrl: './detalleFormaPago.component.html',
  styleUrls: ['./detalleFormaPago.component.sass']
})
export class detalleFormaPagoComponent implements AfterViewInit {

  @Output() validate = new EventEmitter();
  //@ViewChild('captcha') appCaptcha;
  @ViewChild('cvv') cvv;
  @Input('tarjeta') tarjeta   : Tarjeta = new Tarjeta();
  @Input('mensualidad') mensualidad : number = 1;
  @Input('montoPago') montoPago : number = 0;
  @Input('checkout') checkout : Checkout = new Checkout();
  @Input('showCode') showCode : boolean = false;
  @Input('showEdit') showEdit : boolean = false;
  @Input('captcha') captcha   : Captcha = new Captcha();
  @Input('idFormaPago') idFormaPago : number;
  @Input('facturacion') facturacion : Direccion;
  @Input('nombrePromo') nombrePromo : string;
  @Input('cvvHelp') cvvHelp : string;
  @Input('rules') rules = {
    'codigo' : new Validator(
      'codigo',
      'abc',
      'string',
      'El campo codigo es requerido'
    )
  };

  public paymentTitle     : string = 'Tarjeta de crédito o débito';
  public code             : string = '';
  public codeSubscription : Subscription;
  public step4 = false;

  constructor( private globalService : GlobalService, private datalayerService : DatalayerService) { }

  ngAfterViewInit() {
    switch(this.idFormaPago) {
      case SEARS:
      case SANBORNS:
      case SEARSSININTERESES:
      case SEARSREVOLVENTE:
        this.paymentTitle = 'Tarjetas departamentales';
        break;
      case DEBITO:
        this.paymentTitle = 'Tarjeta de crédito o débito';
        break;
      case CREDITO:
        this.paymentTitle = 'Tarjeta de crédito o débito';
        break;
      default:
        this.paymentTitle = 'Tarjeta de crédito o débito';
        break;
    }
    //this.actionCaptcha()
    this.globalService.getCvv()
    this.codeSubscription = this.globalService.cvv
      .subscribe( cvv => {
        this.code = cvv
        if( (/(^[0-9]+)$/.test(cvv)) ){
          this.showCode = false
          this.checkout.codigo = +cvv
        }
      });
  }

  /**
   * Helper Format card
   * Formato digitos
   * @param digitos
   * @param type
   */
  public formatCard(digitos:string, type:string):string {

    let response : string = ''
    let gaps     : number[] = [4, 8, 12];
    let card     : string = '';

    switch (type) {
      case 'amex':
        card = '***********' + digitos
        gaps = [4, 10]
        break;
      case 'mastercard':
      case 'visa':
        card = '************' + digitos;
        gaps = [4, 8, 12]
        break;
      case 'sanborns':
      case 'sears':
        card = '******' + digitos;
        gaps = [2, 8]
        break;
      default:
        card = '************' + digitos;
        gaps = [4, 8, 12]
        break;
    }
    for(var i = 0; i < card.length; i++){
      for(let gap of gaps){
        if(i == gap && i > 0){
          response = response + ' '
        }//end if
      }//end for
      response = response + card[i]
    }//end for

    return response
  }

  /**
   * Helper Imagen tarjeta
   * @param digitos
   * @param type
   */
  public imageCard(type:string):string {
    let response : string;
    switch (type) {
      case 'amex':
        response = './assets/logo-amex.svg';
        break;
      case 'mastercard':
        response = './assets/logo-mastercard.svg';
        break;
      case 'visa':
        response = './assets/logo-visa2.png';
        break;
      case 'sears':
        response = './assets/logo-t-sears1.png';
        break;
      case 'sanborns':
        response = './assets/logo-t-sanborns1.png';
        break;
      default:
        response = './assets/logo-visa2.png';
        break;
    }
    return response
  }

  /**
   *
   */
  //public actionCaptcha():void {
  //  this.appCaptcha.captcha = this.captcha
  //  this.appCaptcha.actionCaptcha()
  //}

  /**
   *
   */
  public focusCvv(): void{
    if(this.showCode == true){
      const element = this.cvv.nativeElement.focus();
      setTimeout(() => element.focus(), 0);
    }
  }

  /**
   * Validate cvv
   */
  public validateCvv(event :any): void{
    if(this.checkout.codigo.toString().length >= 2 ){
      this.validate.emit(null)
    }

    if(this.checkout.codigo.toString().length == 3 && !this.step4 ){
      this.datalayerService.setCheckoutStep4();
      this.step4 = true;
    }
  }

}
