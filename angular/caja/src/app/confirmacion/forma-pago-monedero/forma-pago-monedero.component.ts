import {Component, Input, OnInit} from '@angular/core';
import {Multipedido} from "../../entities/multipedido";

@Component({
  selector: 'app-forma-pago-monedero',
  templateUrl: './forma-pago-monedero.component.html',
  styleUrls: ['./forma-pago-monedero.component.sass']
})
export class FormaPagoMonederoComponent implements OnInit {
  @Input ('multipedido') multipedido : Multipedido = new Multipedido();
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
