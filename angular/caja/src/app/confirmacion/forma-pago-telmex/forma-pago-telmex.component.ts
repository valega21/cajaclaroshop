import { Component, OnInit, Input } from '@angular/core';
import { Multipedido } from '../../entities/multipedido';

@Component({
  selector: 'app-forma-pago-telmex',
  templateUrl: './forma-pago-telmex.component.html',
  styleUrls: ['./forma-pago-telmex.component.sass']
})
export class FormaPagoTelmexComponent implements OnInit {

  @Input('multipedido') multipedido : Multipedido = new Multipedido();
  @Input('mensualidad') mensualidad : number = 0;
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
