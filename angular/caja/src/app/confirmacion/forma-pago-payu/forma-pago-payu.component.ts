import { Component, OnInit, Input } from '@angular/core';
import { Multipedido } from '../../entities/multipedido';

@Component({
  selector: 'app-forma-pago-payu',
  templateUrl: './forma-pago-payu.component.html',
  styleUrls: ['./forma-pago-payu.component.sass']
})
export class FormaPagoPayuComponent implements OnInit {

  @Input ('multipedido') multipedido : Multipedido = new Multipedido();
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
