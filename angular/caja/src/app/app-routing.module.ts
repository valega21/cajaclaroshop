import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DireccionesComponent } from './direcciones/direcciones.component';
import { direccionNuevaComponent } from './direcciones/direccion-nueva/direccion-nueva.component';
import { DireccionesDetalleComponent } from './direcciones/direcciones-detalle/direcciones-detalle.component';
import { formasPagoComponent } from './formas-pago/formas-pago.component';
import { telmexComponent } from './formas-pago/telmex/telmex.component';
import { sinCuentaComponent } from './formas-pago/telmex/sin-cuenta/sin-cuenta.component';
import { tcComponent } from './formas-pago/tc/tc.component';
import { cuentaTelmexComponent } from './formas-pago/telmex/cuenta-telmex/cuenta-telmex.component';
import { mensualidadesTelmexComponent } from './formas-pago/telmex/mensualidades-telmex/mensualidades-telmex.component';
import { DepositoBancarioComponent } from "./formas-pago/deposito-bancario/deposito-bancario.component";
import { tiendasComponent } from "./formas-pago/tiendas/tiendas.component";
import { promocionesComponent } from "./formas-pago/departamental/promociones/promociones.component";
import { ConfirmacionComponent } from "./confirmacion/confirmacion.component";
import { facturacionComponent } from './facturacion/facturacion.component';
import { paypalComponent } from './formas-pago/paypal/paypal.component';
import { tarjetasGuardadasComponent } from './formas-pago/tc/tarjetas-guardadas/tarjetas-guardadas.component';
import { tarjetasMesesComponent } from './formas-pago/tc/tarjetas-mesualidades/tarjetas-mensualidades.component';
import { thankYouPageComponent } from './thankyouPage/ThankYouPage/thankYouPage.component';
import { fraudeComponent } from './fraude/fraude.component';
import { PayuComponent } from './formas-pago/payu/payu.component';
import { ClickRecogeComponent } from './click-recoge/click-recoge.component';
import { AppCsComponent } from './app-cs/app-cs.component'
import { TransferComponent } from './formas-pago/transfer/transfer.component';
import {tarjetaRechazadaComponent} from './formas-pago/tarjetaRechazada/tarjetaRechazada.component';
import { ClickRecogeResolve } from './entities/clickRecogeResolve';
import { ClickRecogeAppResolve } from './entities/clickRecogeAppResolve';
import { InitResolve } from './entities/initResolve'
import { graciasComponent } from './thankyouPage/ThankYouPage/gracias/gracias.component';
import { wishComponent } from './modales/wish/wish.component';
import { cartComponent } from './modales/addCart/addCart.component';
import { errorAppComponent } from './error-app/error-app.component';
import {MonederoComponent} from "./formas-pago/monedero/monedero.component";
import {T1pagosComponent} from "./formas-pago/t1pagos/t1pagos.component";

const routes: Routes = [
  { path: '',     redirectTo: 'direcciones', pathMatch: 'full' },
  { path: 'caja', redirectTo: 'direcciones', pathMatch: 'full' },
  { path: 'Caja', redirectTo: 'direcciones', pathMatch: 'full' },
  { path: 'app/:token', component: AppCsComponent },
  //click recoge
  { path: 'app/click-recoge/:token', component: ClickRecogeComponent, resolve:{ clickRecoge : ClickRecogeAppResolve} },
  { path: 'click-recoge/:cliente/:producto/:sucursal', component: ClickRecogeComponent, resolve:{ clickRecoge : ClickRecogeResolve} },
  { path: 'click-recoge/:cliente/:producto/:sucursal/:skuhijo', component: ClickRecogeComponent, resolve:{ clickRecoge : ClickRecogeResolve} },
  //direcciones
  { path: 'direcciones',               component: DireccionesComponent, resolve:{ direcciones : InitResolve}},
  { path: 'direcciones/detalle/:id',   component: DireccionesDetalleComponent },
  { path: 'direcciones/agregar',       component: direccionNuevaComponent },
  //formas de pago
  { path: 'formas-pago',               component: formasPagoComponent},
  { path: 'formas-pago/guardadas',     component: tarjetasGuardadasComponent},
  //formas de pago credito-debito
  { path: 'formas-pago/tarjetas',      component: tcComponent },
  { path: 'formas-pago/tarjetas/:id',  component: tcComponent },
  { path: 'formas-pago/tarjeta/mensualidades', component: tarjetasMesesComponent },
  //telmex
  { path: 'formas-pago/telmex',             component: telmexComponent},
  { path: 'formas-pago/telmex/sin-cuenta',              component: sinCuentaComponent },
  { path: 'formas-pago/telmex/cuenta-telmex',           component: cuentaTelmexComponent},
  { path: 'formas-pago/telmex/mensualidades',           component: mensualidadesTelmexComponent },
  { path: 'formas-pago/transferencia',      component: DepositoBancarioComponent},
  //tarjetas
  { path: 'formas-pago/departamentales',    component: tcComponent},
  { path: 'formas-pago/departamentales/:id',component: tcComponent},
  { path: 'formas-pago/departamental/promociones', component: promocionesComponent},
  //paypal
  { path: 'formas-pago/paypal', component: paypalComponent },
  { path: 'formas-pago/paypal/:error', component: formasPagoComponent},
  //transfer
  { path: 'formas-pago/transfer', component: TransferComponent },
  //tiendas
  { path: 'formas-pago/tiendas', component: tiendasComponent },
  { path: 'confirmacion', component: ConfirmacionComponent },
  { path: 'confirmacion/:secure', component: ConfirmacionComponent },
  //facturación
  { path: 'facturacion', component: facturacionComponent},
  //thankyouPage
  { path: 'gracias-por-tu-compra', component: thankYouPageComponent },
  { path: 'gracias-por-tu-confianza', component: thankYouPageComponent },
  { path: 'gracias', component: graciasComponent },
  //fraude
  { path: 'error', component: fraudeComponent },
  //Payu
  { path: 'formas-pago/conveniencia', component: PayuComponent },
  //Error en la app
  { path: 'error-app', component: errorAppComponent},
  //modales
  {path: 'wish-list', component: wishComponent},
  {path: 'add-cart', component: cartComponent},
  { path: 'formas-pago/monedero', component: MonederoComponent },
  // T1 pagos
  { path: 'formas-pago/t1pagos', component: T1pagosComponent },
  { path: 'formas-pago/t1pagos/:id',  component: T1pagosComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
