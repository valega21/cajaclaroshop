import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { tap } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ConfigService } from '../services/config.service';
import { GlobalService } from '../services/global.service';
import { Config } from '../entities/config';

@Injectable()

export class TokenInterceptor implements HttpInterceptor {

  /**
   * Config
   */
  private config : Config = new Config();
  private tokenName = 'CSC-KEY';
  private requests: HttpRequest<any>[] = [];
  private authenticationError : boolean = false;
  public isApp : boolean = false;

  /**
   *
   * @param {Router} router
   * @param {ConfigService} configService
   * @param {GlobalService} globalService
   */
  constructor(
      private router: Router,
      private configService: ConfigService,
      private globalService: GlobalService
  ) {
    this.config = this.configService.config
    this.globalService.authenticationError.subscribe(
      error => {
        this.authenticationError = error
      }
    );
  }

  /**
   * Remove request loading
   * @param req
   */
  private removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }//end if
    this.globalService.setLoading(this.requests.length > 0);
  }

  /**
   * Intercept
   * @param req
   * @param next
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let regex = new RegExp(this.configService.getConfig().t1pagosEndpoint + '.+');
    if(regex.test(req.url)){
      return next.handle(req);
    }
    this.requests.push(req);
    this.globalService.setLoading(true);
    let reqClone = req.clone();
    if (localStorage.getItem(this.tokenName) != null && localStorage.getItem(this.tokenName) != 'null' ) {
      reqClone = req.clone({
        headers: req.headers.set(this.tokenName, localStorage.getItem(this.tokenName))
      });
    }
    return Observable.create( observer => {
      const subscription = next.handle(reqClone)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              if(event.headers.get(this.tokenName) != null ) {
                localStorage.setItem(this.tokenName, event.headers.get(this.tokenName) )
              }
              if (event.headers.get(this.tokenName) == 'delete' ){
                localStorage.removeItem(this.tokenName)
              }
              this.removeRequest(req);
              observer.next(event);
            }
          },
          (error : HttpErrorResponse) => {
            this.globalService.isAppCS.subscribe( isApp => {
              this.isApp = isApp;
            });
            if(error instanceof HttpErrorResponse){
              let message = ''
              let redirect = undefined
              switch(error.status){
                case 401:
                  if (this.isApp){
                    redirect = '/error-app';
                    message  = 'Tu sesion ha caducado';
                    this.globalService.setAuthenticationError(true);
                  }else {
                    if (this.authenticationError == false) {
                      redirect = '/direcciones';
                      message  = 'Tu sesion ha caducado';
                      this.globalService.setAuthenticationError(true);
                    }//end if
                  }
                  break;
                case 409:
                  console.log(error.url == this.configService.getConfig().t1pagosEndpoint+'/tarjeta')
                  if (this.isApp){
                    message  = 'Producto sin existencia en el carrito';
                    redirect = '/error-app';
                  } else if(error.url == this.configService.getConfig().t1pagosEndpoint+'/tarjeta'){
                    message  = 'La tarjeta ya existe';
                  } else  {
                    message  = 'Producto sin existencia en el carrito';
                  }
                  break;
                case 500:
                  if (this.isApp){
                    message = "Ocurrio un error intenta mas tarde";
                    redirect = '/error-app';
                  } else {
                    message = 'Ocurrio un error intenta mas tarde';
                  }
                  break;
                default:
                  if (this.isApp){
                    redirect = "/error-app";
                    message = "Ocurrio un error intenta mas tarde";
                  } else {
                    message = 'Ocurrio un error intenta mas tarde';
                  }
                  break;
              }
              let debug = {error : true, message : message, code : error.status}
              console.log(debug)
              if(redirect != undefined){
                this.router.navigate([redirect]);
              }//end if
              this.globalService.setError(true)
              this.globalService.setMessage(message)
              this.globalService.setCode(error.status)
              this.globalService.setLoading(false)
            }
            this.removeRequest(req); observer.error(error);
          },
          () => {
            this.removeRequest(req);
            observer.complete();
          }
        );
      return () => {
        this.removeRequest(req);
        subscription.unsubscribe();
      };
    });
  }
}
