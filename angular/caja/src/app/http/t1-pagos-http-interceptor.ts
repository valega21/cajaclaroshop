import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {ConfigService} from '../services/config.service';
import {GlobalService} from '../services/global.service';
import {Config} from '../entities/config';
import {retryWhen} from "rxjs/operator/retryWhen";
import {delay} from "rxjs/operator/delay";
import {mergeMap} from "rxjs/operator/mergeMap";
import {take} from "rxjs/operator/take";
import { of } from 'rxjs/observable/of';
import {interval} from "rxjs/observable/interval";
import {flatMap} from "tslint/lib/utils";

@Injectable()

export class T1PagosHttpInterceptor implements HttpInterceptor {

  /**
   * Config
   */
  private config: Config = new Config();
  private requests: HttpRequest<any>[] = [];
  private t1pagosError: boolean = false;
  public isApp: boolean = false;

  /**
   *
   * @param {Router} router
   * @param {ConfigService} configService
   * @param {GlobalService} globalService
   */
  constructor(private router: Router,
              private configService: ConfigService,
              private globalService: GlobalService) {
    this.config = this.configService.config
    this.globalService.t1pagosError.subscribe(
      error => {
        this.t1pagosError = error
      }
    );
  }

  /**
   * Remove request loading
   * @param req
   */
  private removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }//end if
    //this.globalService.setLoading(this.requests.length > 0);
  }

  /**
   * Intercept
   * @param req
   * @param next
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let regex = new RegExp(this.configService.getConfig().t1pagosEndpoint + '.+');
    if (!regex.test(req.url)) {
      return next.handle(req);
    }
    //this.globalService.setLoading(true);
    return new Observable<HttpEvent<any>>(subscriber => {
      return next.handle(req)
        .subscribe(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this.removeRequest(req);
            subscriber.next(event);
          }
        },
        (error: HttpErrorResponse) => {
          this.globalService.isAppCS.subscribe(isApp => {
            this.isApp = isApp;
          });
          if (error instanceof HttpErrorResponse) {
            let message = ''
            let redirect = undefined
            switch (error.status) {
              case 401:
                if (this.isApp) {
                  redirect = '/error-app';
                  message = 'T1: Tu sesion ha caducado';
                } else {
                  if (this.t1pagosError == false) {
                    redirect = '/direcciones';
                    message = 'T1: Tu sesion ha caducado';
                  }//end if
                }
                break;
              case 412:
                if (this.isApp) {
                  redirect = '/error-app';
                  message = 'T1: Verifica que todos los datos de tu tarjeta sean válidos';
                } else {
                  if (this.t1pagosError == false) {
                    message = 'T1: Verifica que todos los datos de tu tarjeta sean válidos';
                  }//end if
                }
                break;
              case 404:
                if (this.isApp) {
                  redirect = '/error-app';
                  message = 'T1: Tarjeta no encontrada';
                } else {
                  if (this.t1pagosError == false) {
                    redirect = '/direcciones';
                    message = 'T1: Tarjeta no encontrada';
                  }//end if
                }
                break;
              case 409:
                message = 'T1: La tarjeta que estás intentando agregar ya existe';
                break;
              case 429:
                message = 'T1: Por favor intenta de nuevo más tarde.'
              case 500:
                if (this.isApp) {
                  message = "T1: Ocurrió un error, intenta más tarde";
                  redirect = '/error-app';
                } else {
                  this.globalService.setError(true);
                  message = 'T1: Ocurrió un error, intenta más tarde';
                }
                break;
              default:
                if (this.isApp) {
                  redirect = "/error-app";
                  message = "T1: Ocurrió un error, intenta mas tarde";
                } else {
                  message = 'T1: Ocurrió un error, intenta mas tarde';
                }
                break;
            }
            let debug = {error: true, message: message, code: error.status}
            console.log(debug)
            if (redirect != undefined) {
              this.router.navigate([redirect]);
            }//end if
            //this.globalService.setLoading(false)
            this.globalService.setT1PagosError(true);
            this.globalService.setMessage(message)
            this.globalService.setCode(error.status)
          }
          this.removeRequest(req);
          subscriber.error(error);
        },
        () => {
          this.removeRequest(req);
          subscriber.complete();
        })
    });
  }
}
