// angular
import { AppRoutingModule } from './/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormBuilder, FormsModule, FormGroup, Validators } from "@angular/forms";
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CookieService } from 'ngx-cookie-service';
import { UICarouselModule } from 'ui-carousel'

// Componentes
import { AppComponent } from './app.component';
import { DireccionesComponent } from './direcciones/direcciones.component';
import { DireccionesDetalleComponent } from './direcciones/direcciones-detalle/direcciones-detalle.component';
import { detalleCompraComponent } from './detalleCompra/detalleCompra.component';
import { direccionNuevaComponent } from './direcciones/direccion-nueva/direccion-nueva.component';
import { CaptchaComponent } from './captcha/captcha.component';
import { spinnerComponent } from './spinner/spinner.component';
import { pathComponent } from './path/path.component';
import { formasPagoComponent } from './formas-pago/formas-pago.component';
//usuarios
import { usuarioComponent } from './confirmacion/usuario/usuario.component';
//telmex
import { telmexComponent } from './formas-pago/telmex/telmex.component';
import { sinCuentaComponent } from './formas-pago/telmex/sin-cuenta/sin-cuenta.component';
import { cuentaTelmexComponent } from './formas-pago/telmex/cuenta-telmex/cuenta-telmex.component';
import { mensualidadesTelmexComponent } from './formas-pago/telmex/mensualidades-telmex/mensualidades-telmex.component';
import { tiendasComponent } from './formas-pago/tiendas/tiendas.component';
//Tarjetas
import { tcComponent } from './formas-pago/tc/tc.component';
import { tarjetasGuardadasComponent } from './formas-pago/tc/tarjetas-guardadas/tarjetas-guardadas.component';
import { tarjetasMesesComponent } from './formas-pago/tc/tarjetas-mesualidades/tarjetas-mensualidades.component';
import { CardModule } from 'ngx-card/ngx-card';
//paypal
import { paypalComponent } from './formas-pago/paypal/paypal.component';

//Gracias
import { thankyouPageComponent } from './thankyouPage/thankyouPage.component';
import { thankYouPageComponent } from './thankyouPage/ThankYouPage/thankYouPage.component';
// Servicios
import { TuCompraService } from "./services/tuCompra.service";
import { DireccionesService } from './services/direcciones.service';
import { ZonificacionService } from './services/zonificacion.service';
import { DepositoService } from "./services/deposito.service";
import { CuponService } from "./services/cupon.service";
import { ConfirmacionDepositoBancarioComponent } from './confirmacion/deposito-bancario/deposito-bancario.component';
import { DepositoBancarioComponent } from "./formas-pago/deposito-bancario/deposito-bancario.component";
import { ConfirmacionComponent } from './confirmacion/confirmacion.component';
import { DireccionEnvioComponent } from './confirmacion/direccion-envio/direccion-envio.component';
import { CuponComponent } from './confirmacion/cupon/cupon.component';
import { promocionesComponent } from './formas-pago/departamental/promociones/promociones.component';
import { facturacionComponent } from './facturacion/facturacion.component';
import { detalleFormaPagoComponent } from './confirmacion/detalleFormaPago/detalleFormaPago.component';
import { ValidatorService } from './services/validator.service';
import { FormasDePagoService } from './services/formas-de-pago.service';
import { TarjetasCreditoDebitoService } from './services/tarjetas-credito-debito.service';
import { CardValidatorService } from './services/card-validator.service';
import { MensualidadesService } from './services/mensualidades.service';
import { MultipedidoService } from './services/multipedido.service';
import { DeviceFingerPrintService } from './services/device-finger-print.service';
import { PaymentCreditoDebitoService } from './services/payment-credito-debito.service';
import { PaymentDepositoService } from "./services/payment-deposito.service";
import { PaymentPagoTiendaService } from './services/payment-pago-tienda.service';
import { PaymentPaypalService } from './services/payment-paypal.service';
import { PaymentPayuService } from './services/payment-payu.service';
import { PaymentTelmexService } from './services/payment-telmex.service';
import { PromocionesService } from "./services/promociones.service";
import { TelmexService } from "./services/telmex.service";
import { MensualidadesTelmexService } from "./services/mensualidades-telmex.service";
import { InitCajaService } from "./services/init-caja.service";
import { ConfigService } from "./services/config.service";
import { ErrorService } from "./services/error.service";
import { GlobalService } from "./services/global.service";
import { DatalayerService } from "./services/datalayer.service";
import { ClickRecogeService } from './services/click-recoge.service';
import { SucursalesService } from './services/sucursales.service';
import { PaymentTransferService } from './services/payment-transfer.service';
import { CarritoService } from "./services/carrito.service";
//botones
import { editarComponent } from './boton-editar/boton-editar.component';
import { eliminarComponent } from './boton-eliminar/boton-eliminar.component';
import { especificacionFormaPagoComponent } from './thankyouPage/ThankYouPage/deposito/especificacionFormaPago.component';
import { DetalleTotalesComponent } from './detalleCompra/detalleTotales/detalleTotales.component';
import { ProductosComponent } from './detalleCompra/productos/productos.component';
import { CreditoDebitoComponent } from './thankyouPage/ThankYouPage/credito-debito/credito-debito.component';
import { TiendaDepartamentalComponent } from './confirmacion/tienda-departamental/tienda-departamental.component';
import { TiendasThanckYouComponent } from './thankyouPage/ThankYouPage/tiendas/tiendas.component';
import { fraudeComponent } from './fraude/fraude.component';
import { FormaPagoPaypalComponent } from './confirmacion/forma-pago-paypal/forma-pago-paypal.component';
import { ThanckyouPaypalComponent } from './thankyouPage/ThankYouPage/thanckyou-paypal/thanckyou-paypal.component';
import { PayuComponent } from './formas-pago/payu/payu.component';
import { FormaPagoPayuComponent } from './confirmacion/forma-pago-payu/forma-pago-payu.component';
import { ThanckyouPayuComponent } from './thankyouPage/ThankYouPage/thanckyou-payu/thanckyou-payu.component';
import { PaymentTarjetaDepartamentalService } from "./services/payment-tarjeta-departamental.service";
import { DepartamentalesComponent } from './thankyouPage/ThankYouPage/departamentales/departamentales/departamentales.component';
import { FormaPagoTelmexComponent } from './confirmacion/forma-pago-telmex/forma-pago-telmex.component';
import { ThanckyouTelmexComponent } from './thankyouPage/ThankYouPage/thanckyou-telmex/thanckyou-telmex.component';
import { ThanckyouConektaComponent } from './thankyouPage/ThankYouPage/thanckyou-conekta/thanckyou-conekta.component';

import { TokenInterceptor } from './http/token';
import { InitResolve } from './entities/initResolve';
import { ClickRecogeResolve } from './entities/clickRecogeResolve';
import { ClickRecogeAppResolve } from './entities/clickRecogeAppResolve';
import { MessageComponent } from './message/message.component';
import { PixelComponent } from './pixel/pixel.component';
import { FacebookPixelComponent } from './pixel/facebook-pixel/facebook-pixel.component';
import { ClickRecogeComponent } from './click-recoge/click-recoge.component';
import { SucursalComponent } from './click-recoge/sucursal/sucursal.component';
import { DireccionFacturacionComponent } from './confirmacion/direccion-facturacion/direccion-facturacion.component';
import { AppCsComponent } from './app-cs/app-cs.component';
import { TransferComponent } from './formas-pago/transfer/transfer.component';
import { FormaPagoTransferComponent } from './confirmacion/forma-pago-transfer/forma-pago-transfer.component';
import { ThanckyouTransferComponent } from './thankyouPage/ThankYouPage/thanckyou-transfer/thanckyou-transfer.component';
import { tarjetaRechazadaComponent } from './formas-pago/tarjetaRechazada/tarjetaRechazada.component';
import { EmarsysComponent } from './thankyouPage/ThankYouPage/emarsys/emarsys.component';
import { EmarsysService } from './services/emarsys.service';
import { errorAppComponent } from './error-app/error-app.component';
import { TyHeaderComponent } from './thankyouPage/parts/ty-header/ty-header.component';
import { TyArticulosCompradosComponent } from './thankyouPage/parts/ty-articulos-comprados/ty-articulos-comprados.component';
import { TyRelacionadosComponent } from './thankyouPage/parts/ty-relacionados/ty-relacionados.component';
import { TyFooterComponent } from './thankyouPage/parts/ty-footer/ty-footer.component';
import { graciasComponent } from './thankyouPage/ThankYouPage/gracias/gracias.component';

//modales
import { wishComponent } from './modales/wish/wish.component';
import { cartComponent } from './modales/addCart/addCart.component';
import { UILazyloadDirective } from './thankyouPage/ThankYouPage/carousel/directives/ui-lazy-load.directive';
import { SwiperDirective } from './thankyouPage/ThankYouPage/carousel/directives/swiper.directive';
import { DotsComponent } from './thankyouPage/ThankYouPage/carousel/dots/dots.component';
import { UICarouselComponent } from './thankyouPage/ThankYouPage/carousel/ui-carousel/ui-carousel.component';
import { UICarouselItemComponent } from './thankyouPage/ThankYouPage/carousel/ui-carousel-item/ui-carousel-item.component';
import { ArrowComponent } from './thankyouPage/ThankYouPage/carousel/arrow/arrow.component';
import { MonederoComponent } from './formas-pago/monedero/monedero.component';
import { PaymentMonederoService } from "./services/payment-monedero.service";
import { FormaPagoMonederoComponent } from './confirmacion/forma-pago-monedero/forma-pago-monedero.component';
import { T1pagosComponent } from "./formas-pago/t1pagos/t1pagos.component";
import { T1TarjetasService } from "./services/t1-tarjetas.service";
import { TarjetaPagosService } from "./services/tarjeta-pagos.service";
import { T1ClientesService } from "./services/t1-clientes.service";
import { PaymentT1PagosService } from "./services/payment-t1-pagos.service";
import { T1PagosHttpInterceptor } from "./http/t1-pagos-http-interceptor";
const appInitializerFn = (config: ConfigService) => {
  return () => {
    return config.loadAppConfig();
  };
};

@NgModule({
  declarations: [
    AppComponent,
    pathComponent,
    DireccionesComponent,
    usuarioComponent,
    DireccionesDetalleComponent,
    detalleCompraComponent,
    direccionNuevaComponent,
    formasPagoComponent,
    CaptchaComponent,
    telmexComponent,
    mensualidadesTelmexComponent,
    cuentaTelmexComponent,
    sinCuentaComponent,
    ConfirmacionDepositoBancarioComponent,
    tiendasComponent,
    ConfirmacionComponent,
    DireccionEnvioComponent,
    CuponComponent,
    tcComponent,
    spinnerComponent,
    DepositoBancarioComponent,
    promocionesComponent,
    thankyouPageComponent,
    thankYouPageComponent,
    facturacionComponent,
    detalleFormaPagoComponent,
    editarComponent,
    eliminarComponent,
    especificacionFormaPagoComponent,
    paypalComponent,
    DetalleTotalesComponent,
    tarjetasGuardadasComponent,
    tarjetasMesesComponent,
    ProductosComponent,
    CreditoDebitoComponent,
    TiendaDepartamentalComponent,
    TiendasThanckYouComponent,
    fraudeComponent,
    FormaPagoPaypalComponent,
    ThanckyouPaypalComponent,
    PayuComponent,
    FormaPagoPayuComponent,
    ThanckyouPayuComponent,
    DepartamentalesComponent,
    MessageComponent,
    PixelComponent,
    FacebookPixelComponent,
    FormaPagoTelmexComponent,
    ThanckyouTelmexComponent,
    ClickRecogeComponent,
    SucursalComponent,
    DireccionFacturacionComponent,
    AppCsComponent,
    ThanckyouTelmexComponent,
    TransferComponent,
    FormaPagoTransferComponent,
    ThanckyouTransferComponent,
    tarjetaRechazadaComponent,
    EmarsysComponent,
    errorAppComponent,
    graciasComponent,
    TyHeaderComponent,
    TyArticulosCompradosComponent,
    TyRelacionadosComponent,
    TyFooterComponent,
    wishComponent,
    cartComponent,
    errorAppComponent,
    graciasComponent,
    SwiperDirective,
    DotsComponent,
    UILazyloadDirective,
    UICarouselComponent,
    UICarouselItemComponent,
    ArrowComponent,
    ThanckyouConektaComponent,
    MonederoComponent,
    FormaPagoMonederoComponent,
    T1pagosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    FormsModule,
    CardModule,
    // UICarouselModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [ConfigService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: T1PagosHttpInterceptor,
      multi: true
    },
    GlobalService,
    DireccionesService,
    TuCompraService,
    FormBuilder,
    DepositoService,
    ZonificacionService,
    ValidatorService,
    FormasDePagoService,
    TarjetasCreditoDebitoService,
    CardValidatorService,
    MensualidadesService,
    MultipedidoService,
    DeviceFingerPrintService,
    PaymentCreditoDebitoService,
    PaymentDepositoService,
    PaymentPagoTiendaService,
    PaymentPaypalService,
    PaymentPayuService,
    PaymentTelmexService,
    PromocionesService,
    PaymentTarjetaDepartamentalService,
    CookieService,
    InitCajaService,
    InitResolve,
    CuponService,
    ErrorService,
    DatalayerService,
    TelmexService,
    MensualidadesTelmexService,
    CuponService,
    ClickRecogeService,
    SucursalesService,
    ClickRecogeResolve,
    PaymentTransferService,
    EmarsysService,
    CarritoService,
    ClickRecogeAppResolve,
    PaymentMonederoService,
    T1TarjetasService,
    T1ClientesService,
    TarjetaPagosService,
    PaymentT1PagosService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
