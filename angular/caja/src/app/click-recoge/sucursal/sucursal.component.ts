import { Component, Input, OnInit} from '@angular/core';
import { Sucursal } from '../../entities/sucursal';

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.sass']
})
export class SucursalComponent implements OnInit {

  @Input('sucursal') sucursal : Sucursal = new Sucursal();

  constructor() { }

  ngOnInit() {
  }
}
