import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SucursalesService } from '../services/sucursales.service';
import { Sucursal } from '../entities/sucursal';
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

@Component({
  selector: 'app-click-recoge',
  templateUrl: './click-recoge.component.html',
  styleUrls: ['./click-recoge.component.sass']
})

export class ClickRecogeComponent implements OnInit {

  public loading    : boolean  = true;
  public sucursal   : Sucursal = new Sucursal();
  public sucursales : Sucursal[];
  public title      : string   = 'Dirección de recopilación';
  public show       : boolean  = false;
  public idSucursal : number ;
  public idProducto : number ;
  public list       : Sucursal[];
  public error      : string;
  public config     : Config = new Config();
  public url        : string;
  public ruta       : string = 'direccionActiva';
  public initializedSubscription : Subscription;

  @Input() selected : number = 0;
  
  constructor(
    private sucursalesService : SucursalesService,
    private route: ActivatedRoute,
    private router: Router,
    private globalService : GlobalService,
    private configService : ConfigService
  ) {
    this.config = this.configService.config
  }

  ngOnInit() {
    this.idSucursal = +this.route.snapshot.paramMap.get('sucursal');
    this.idProducto = +this.route.snapshot.paramMap.get('producto');
    this.url    = this.config.baseUrl + 'producto/' + this.idProducto
    this.getSucursal(this.idSucursal);
  }

  /**
   * 
   */
  public save(): void{
    this.setSucursal(this.idSucursal)
  }

  /**
   * Mustra las sucursales disponibles para la compra
   */
  public showSucursales(){
    if(this.sucursales === undefined ){
      this.getSucursales()
    }
    this.show =! this.show
  }

  /**
   * Cambia el id de sucursal
   * @param id 
   */
  public changeSucursal(id : number): void{
    this.idSucursal = id
    this.sucursal = this.sucursales.find( x => x.id == id )
    this.show =! this.show
  }

  /**
   * Obtiene el objeto sucursal por medio del id
   */
  private getSucursal(id : number): void{
    
    this.sucursalesService.getSucursal(id)
      .subscribe((sucursal: Sucursal ) => {
        this.sucursal = sucursal;
        
      });
  }

  /**
   * Obtiene las sucursales en las que se encuntra disponible el producto
   */
  private getSucursales(): void{
    
    this.sucursalesService.getSucursales()
      .subscribe( sucursales => {
        this.sucursales = sucursales
        this.list = sucursales
        
      });
  }

  /**
   * Define la sucursal
   * @param id 
   */
  private setSucursal(id : number): void{
    
    this.sucursalesService.setSucursal(id)
      .subscribe(response => {
        
        this.processResponse(response)
      });
  }

  /**
   * Filtra sucursales xid
   * @param id 
   */
  public findSucursal( ): void{
    let id : number = this.selected
    if (id > 0){
      let sucursal : Sucursal = this.sucursales.find( x => x.id == id );
      this.list = [sucursal]
    }else {
      this.list = this.sucursales
    }
  }

  /**
   * flujo de estado 
   * @param response 
   */
  private processResponse(response: any): void{
    if(response.estatus == true){
      this.router.navigate(['/formas-pago/guardadas']);
    }else{
      this.error = response.error
    }//end if
  }
}