import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Emarsys} from "../../../entities/emarsys";
import {EmarsysService} from "../../../services/emarsys.service";
import {Config} from "../../../entities/config";
import {ConfigService} from "../../../services/config.service";

@Component({
  selector: 'app-ty-relacionados',
  templateUrl: './ty-relacionados.component.html',
  styleUrls: ['./ty-relacionados.component.sass' ]
})
export class TyRelacionadosComponent implements OnInit {

  @Input('emarsys') emarsys;
  public products     : Emarsys[]
  public showProducts : boolean = false;
  public items        : string = ''
  public config       : Config = new Config()
  public basePath     : string = ''

  constructor(
    private emarsysService : EmarsysService,
    private configService : ConfigService
  ){
    this.config   = this.configService.config;
    this.basePath = this.config.baseUrl + 'producto/'
  }

  ngOnInit() {
  }

  /**
   * Get recomendados emarsys
   */
  public getRecomendados(data : any | undefined): void{
    if(data == undefined){
      data = this.emarsys
    }//end if
    this.emarsysService.getRecomendados(data)
      .subscribe((response : Emarsys[]) =>{
        this.products = response
      })
    this.showProducts = true
  }

}
