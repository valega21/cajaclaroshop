import { Component, OnInit } from '@angular/core';
import { Config } from '../entities/config';
import { ConfigService } from '../services/config.service';

@Component({
  selector: 'app-thankyouPage',
  templateUrl: './thankyouPage.component.html',
  styleUrls: ['./thankyouPage.component.sass']
})
export class thankyouPageComponent implements OnInit {

  private config  : Config = new Config;
  public title    : string = 'Tu compra fue exitosa';
  public subtitle : string = 'Gracias por confiar en nosotros';
  public baseUrl  : string = this.config.baseUrl
  

  constructor(
    private configService : ConfigService
  ) { 
    this.config  = this.configService.config
    this.baseUrl = this.config.baseUrl
  }

  ngOnInit() {
    window.scrollTo(0,0);
  }

}
