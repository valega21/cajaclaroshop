import {Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import { EmarsysService } from '../../../services/emarsys.service'
import { ConfigService } from '../../../services/config.service'
import { Config } from '../../../entities/config'
import { Emarsys } from '../../../entities/emarsys'
import {CarritoService} from "../../../services/carrito.service";
import {Carrito} from "../../../entities/carrito";
import {Sucursal} from "../../../entities/sucursal";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-emarsys',
  templateUrl: './emarsys.component.html',
  styleUrls: ['./emarsys.component.sass']
})
export class EmarsysComponent implements OnInit, AfterViewInit {

  @Input('emarsys') emarsys;
  @ViewChild('carousel') carousel ;
  public products     : Emarsys[]
  public showProducts : boolean = false;
  public items        : string = ''
  public config       : Config = new Config()
  public basePath     : string = ''
  public isActive     : false

  constructor(
    private emarsysService : EmarsysService,
    private configService  : ConfigService,
    private eRef           : ElementRef,
    private carritoService : CarritoService,
    private cookieService  : CookieService
  ){
    this.config   = this.configService.config
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  /**
   * Get recomendados emarsys
   */
  public getRecomendados(data : any | undefined): void{
    if(data == undefined){
      data = this.emarsys
    }//end if
    this.emarsysService.getRecomendados(data)
    .subscribe((response : Emarsys[]) =>{
      this.products = response
    })
    this.showProducts = true
  }

  /**
   * Agrega un producto a la wishlist
   * @param $event
   * @param id
   */
  public addToWishlist ($event, id) {
    $event.preventDefault();
    let carrito = new Carrito();
    let corazon = $event.srcElement;
    // Si no ha sido agregado, lo agrega
    if(!$event.srcElement.classList.contains('active')){
      corazon.classList.add('active');
      /*$.fancybox.open({
        'href': '#modal-addWishlist',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
      });*/
      console.log('se agregó active' + corazon)
    } else {
      // Borra producto de la wishlist
      this.deleteFromWishlist(id);
      //$.fancybox.close();
      // Quita la clase de active
      corazon.classList.remove('active');
    }
  }

  /**
   * Borra un producto del wishlist
   * @param id
   */
  public deleteFromWishlist(id) {

    console.log('wishListeliminar ' + id);
  }

  /**
   * Agrega un producto al carrito
   * @param $event
   * @param id
   */
  public addToCart($event, id) {
    $event.preventDefault();
    let carrito = new Carrito();
    //carrito.producto = id;
    let tokenCart     = this.cookieService.get(this.config.cookieName);
    let tokenLogin    = this.cookieService.get(this.config.cookieLogin);
    carrito.token     = tokenLogin;
    carrito.idCarrito = tokenCart;
    this.carritoService.getCart(carrito).subscribe((carrito) => {
      console.log(carrito)
      console.log('entre a addToCart')

    });

  }

}
