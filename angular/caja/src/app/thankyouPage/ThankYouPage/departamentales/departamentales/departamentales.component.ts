import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-departamentales',
  templateUrl: './departamentales.component.html',
  styleUrls: ['./departamentales.component.sass']
})
export class DepartamentalesComponent implements OnInit {

  @Input('data') data ;

  constructor() { }

  ngOnInit() {
  }

}
