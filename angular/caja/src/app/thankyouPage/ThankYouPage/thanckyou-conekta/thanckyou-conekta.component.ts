import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-thanckyou-conekta',
    templateUrl: './thanckyou-conekta.component.html',
    styleUrls: ['./thanckyou-conekta.component.sass']
})
export class ThanckyouConektaComponent implements OnInit {
    @Input ('data') data ;
    constructor() { }

    ngOnInit() {
    }

}