import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-thanckyou-telmex',
  templateUrl: './thanckyou-telmex.component.html',
  styleUrls: ['./thanckyou-telmex.component.sass']
})
export class ThanckyouTelmexComponent implements OnInit {

  @Input('data') data ;

  constructor() { }

  ngOnInit() {
  }

}
