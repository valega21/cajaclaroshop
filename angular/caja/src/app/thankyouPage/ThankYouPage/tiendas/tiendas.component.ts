import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tiendas-thankyou',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.sass']
})

export class TiendasThanckYouComponent implements OnInit {

  @Input('data') data;

  constructor() { }

  ngOnInit() {
  }

}
