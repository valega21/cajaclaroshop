import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-thanckyou-payu',
  templateUrl: './thanckyou-payu.component.html',
  styleUrls: ['./thanckyou-payu.component.sass']
})

export class ThanckyouPayuComponent implements OnInit, AfterViewInit {

  @Input('data') data ;

  public url;

  constructor(
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {}

  ngAfterViewInit(){
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.pagina)
  }

}