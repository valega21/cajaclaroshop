import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-credito-debito',
  templateUrl: './credito-debito.component.html',
  styleUrls: ['./credito-debito.component.sass']
})
export class CreditoDebitoComponent implements OnInit {

  @Input('data') data ;

  constructor() { }

  ngOnInit() {
  }

}
