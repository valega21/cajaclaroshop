import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentCreditoDebitoService } from '../../services/payment-credito-debito.service';
import { PaymentDepositoService } from "../../services/payment-deposito.service";
import { PaymentPagoTiendaService } from '../../services/payment-pago-tienda.service';
import { PaymentPaypalService } from '../../services/payment-paypal.service';
import { PaymentPayuService } from '../../services/payment-payu.service';
import { PaymentTelmexService } from '../../services/payment-telmex.service';
import { Multipedido } from '../../entities/multipedido';
import { MultipedidoService } from '../../services/multipedido.service';
import { PaymentTarjetaDepartamentalService } from "../../services/payment-tarjeta-departamental.service";
import { ConfigService } from '../../services/config.service';
import { Config } from '../../entities/config';
import { GlobalService } from '../../services/global.service';
import { PaymentTransferService } from '../../services/payment-transfer.service';
import { CookieService } from 'ngx-cookie-service';
import {PaymentMonederoService} from "../../services/payment-monedero.service";
import {PaymentT1PagosService} from "../../services/payment-t1-pagos.service";

@Component({
  selector: 'app-detalle-compra',
  templateUrl: './thankYouPage.component.html',
  styleUrls: ['./thankYouPage.component.sass']
})

export class thankYouPageComponent implements OnInit {

  @ViewChild('emarsysComponent') emarsysComponent ;
  private config      : Config  = new Config();
  public title        : string  = 'Tu compra fue exitosa';
  public subtitle     : string  = 'Gracias por confiar en nosotros';
  public loading      : boolean = false;
  public show         : boolean = true;
  public data         : any;
  public baseUrl      : string  = this.config.baseUrl;
  public multipedido  : Multipedido;
  public idFormaPago  : number  = 0;
  public pixelData    : any;
  public fbPixel      : any;
  public isLoad       : boolean = false;
  public emarsys      : any;
  public google       : any;
  public isApp : boolean = false;
  public tienda       : string;
  public isDebitoCredito    = false;
  public isDepositoBancario = false;
  public isPagoEnTienda     = false;
  public isSanborns         = false;
  public isSears            = false;
  public isPaypal           = false;
  public isPayu             = false;
  public isSevenEleven      = false;
  public isOxxo             = false;
  public isTarjetaDepartamental = false;
  public isTelmex           = false;
  public isTransfer         = false;
  public hidden : boolean = true;
  public isMonedero         = false;
  public isT1Pagos          = false;
  public idFpT1pagos  : number;
  public idFpT1pagosAmex : number;
  /**
   *
   * @param {PaymentCreditoDebitoService} paymentService
   * @param {PaymentDepositoService} paymentDepositoService
   * @param {PaymentPagoTiendaService} paymentPagoTiendaService
   * @param {PaymentPaypalService} paymentPaypalService
   * @param {PaymentPayuService} paymentPayuService
   * @param {PaymentTelmexService} paymentTelmexService
   * @param {MultipedidoService} multipedidoService
   * @param {PaymentTarjetaDepartamentalService} paymentDepartamental
   * @param {ConfigService} configService
   * @param {GlobalService} globalService
   * @param {PaymentTransferService} paymentTransferService
   * @param {CookieService} cookieService
   * @param {PaymentMonederoService} paymentMonedero
   * @param {PaymentT1PagosService} paymentT1Pagos
   */
  constructor(
    private paymentService            : PaymentCreditoDebitoService,
    private paymentDepositoService    : PaymentDepositoService,
    private paymentPagoTiendaService  : PaymentPagoTiendaService,
    private paymentPaypalService      : PaymentPaypalService,
    private paymentPayuService        : PaymentPayuService,
    private paymentTelmexService      : PaymentTelmexService,
    private multipedidoService        : MultipedidoService,
    private paymentDepartamental      : PaymentTarjetaDepartamentalService,
    private configService             : ConfigService,
    private globalService             : GlobalService,
    private paymentTransferService    : PaymentTransferService,
    private cookieService             : CookieService,
    private paymentMonedero           : PaymentMonederoService,
    private paymentT1Pagos            : PaymentT1PagosService
  ){
    this.config  = this.configService.config;
    this.baseUrl = this.config.baseUrl;
    this.idFpT1pagos = this.config.t1pagosId;
    this.idFpT1pagosAmex = this.config.t1pagosAmex;
   }


  ngOnInit() {
    this.arriba();
    this.getMultipedido();
    this.globalService.isAppCS.subscribe( isApp => {
      this.isApp = isApp;
    });
  }

  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido():void {

    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
          this.multipedido = multipedido;
          this.idFormaPago = multipedido.idFormaDePago;
          this.getData()
          this.isLoad = true
      });
  }

  /**
   * Obtiene los parametros del pedido
   */
  private getData():void {

    switch (this.idFormaPago) {
      case 344:
      case 3912:
        this.paymentService.getThankYouPageData()
        .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie();
            this.isDebitoCredito =true;
            this.hidden = false;
        });
      break;
      case 4:
        this.paymentDepositoService.getThankYouPageData()
        .subscribe(data => {
          this.data       = data;
          this.pixelData  = this.data.pixeles;
          this.fbPixel    = this.data.fbPixel;
          this.emarsys    = this.data.emarsys;
          this.google     = this.data.google;
          this.emarsysComponent.getRecomendados(this.data.emarsys)
          this.globalService.setInitialized(true)
          this.deleteCartCookie();
          this.isDepositoBancario = true;
          this.hidden = false;
        });
      break;
      case 4450:
        this.paymentPagoTiendaService.getThankYouPageData()
        .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie();
            this.isPagoEnTienda = true;
            if(this.multipedido.formaDePago==11){
              this.tienda = 'SANBORNS';
              this.isSanborns = true;
            } else {
              this.tienda = 'SEARS';
              this.isSears = true;
            }
            this.hidden = false;
        });
        break;
      case 5:
        this.paymentPaypalService.getThankYouPageData()
        .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie();
            this.isPaypal = true;
            this.hidden = false;
        });
        break;
      case 1389:
      case 4465:
        this.paymentPayuService.getThankYouPageData()
        .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie();
            this.isPayu = true;
            if(this.multipedido.formaDePago == 2){
              this.tienda = 'SEVEN ELEVEN';
              this.isSevenEleven = true;
            } else {
              this.tienda = 'OXXO';
              this.isOxxo = true;
            }
            this.hidden = false;
        });
      break;
      case 4384:
      case 4385:
      case 4375:
      case 4379:
        this.paymentDepartamental.getThankYouPageData()
          .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie()
            this.isTarjetaDepartamental = true;
            this.hidden = false;
          });
        break;
      case 3158:
        this.paymentTelmexService.getThankYouPageData()
          .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie()
            this.isTelmex = true;
            this.hidden = false;
          });
        break;
      case 4448:
        this.paymentTransferService.getThankYouPageData()
          .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie()
            this.isTransfer = true;
            this.hidden = false;
          });
        break;
      case 4466:
        this.paymentMonedero.getThankYouPageData()
          .subscribe(data => {
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie()
            this.isMonedero = true;
            this.hidden = false;
          });
        break;
      case this.idFpT1pagos:
        case this.idFpT1pagosAmex:
        this.paymentT1Pagos.getThankYouPageData()
          .subscribe(data =>{
            this.data       = data;
            this.pixelData  = this.data.pixeles;
            this.fbPixel    = this.data.fbPixel;
            this.emarsys    = this.data.emarsys;
            this.google     = this.data.google;
            this.emarsysComponent.getRecomendados(this.data.emarsys)
            this.globalService.setInitialized(true)
            this.deleteCartCookie()
            this.isT1Pagos = true;
            this.isDebitoCredito =true;
            this.hidden = false;
          });
    }//end switch

  }

  /**
   * Arriba
   */
  public arriba(): void{
    window.scrollTo(0,0);
  }

  /**
   * Calcula el descuento de un producto
   * @param precio
   * @param costo
   */
  public calcDesc(precio: number, costo: number): number{
    let descuento = Math.round((precio * 100)/costo)
    descuento = 100 - descuento
    return descuento
  }

  /**
   *
   * @param url
   */
  public goHome(url): void{
      window.location.href = url;
  }

  /**
   * Delete cart cookie
   */
  private deleteCartCookie(): void{
    this.cookieService.delete(this.config.cookieName)
  }

}
