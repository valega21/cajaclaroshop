import { Component, OnInit } from '@angular/core';
import { ResumenCompra } from "../../entities/resumenCompra";
import { TuCompraService } from "../../services/tuCompra.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-detalleProductos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.sass']
})
export class ProductosComponent implements OnInit {

  public detalleCompra : ResumenCompra;
  public detalleCompraSubscription : Subscription;
  public productos;
  public productosKeys;
  constructor(public tuCompraService: TuCompraService) { }

  ngOnInit(){
    // Obtengo los datos del Resumen Compra para mostrar los totales
    this.detalleCompraSubscription = this.tuCompraService.rcActual.subscribe((resumenCompra: ResumenCompra) => {
      this.detalleCompra = resumenCompra;
      this.productos = resumenCompra != undefined ? Object.values(resumenCompra.productos) : [];
    })
  }

}