import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fraude',
  templateUrl: './fraude.component.html',
  styleUrls: ['./fraude.component.sass']
})

export class fraudeComponent implements OnInit {

  public error   : boolean = false;
  public errorSubscription : Subscription;
  public message : string  = '';
  public messageSubscription : Subscription;
  public action  : number ;

  /**
   * Global service
   * @param globalService 
   */
  constructor(
    private globalService : GlobalService
  ) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.errorSubscription = this.globalService.error.subscribe( error => {
      this.error = error;
    });
    this.messageSubscription = this.globalService.message.subscribe( message => {
      this.message = message;
    });

  }

  /**
   * 
   */
  public hide() : void{
    this.globalService.setError(false)
    this.globalService.setMessage('')
  }

}
