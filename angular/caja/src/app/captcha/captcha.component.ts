import { AfterViewInit, Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ReCAPTCHA } from "../ReCAPTCHA.interface";
import { Captcha } from '../entities/captcha';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

declare var grecaptcha: ReCAPTCHA;

@Component({
  selector    : 'app-captcha',
  templateUrl : './captcha.component.html',
  styleUrls   : ['./captcha.component.sass']
})

export class CaptchaComponent implements AfterViewInit, OnInit {

  @Output() validate = new EventEmitter();
  @ViewChild('captcha') divCaptcha;
  @ViewChild('delay') delay;
  @Input('captcha') captcha : Captcha = new Captcha();

  public created : number = 0;
  public attempts: number = 0;
  public config  : Config = new Config();
  public sitekey : string ;

  constructor(
    private router: Router,
    private configService : ConfigService
  ) {
    this.config = this.configService.config
    this.sitekey = this.config.apiKeyCaptcha
  }

  ngAfterViewInit() {
    this.actionCaptcha()
  }

  ngOnInit(){
    window['verifyCaptcha']   = this.verifyCaptcha.bind(this);
    window['declinedCaptcha'] = this.declinedCaptcha.bind(this);
    window['actionCaptcha']   = this.createCaptcha.bind(this);
  }

  /**
   * Verify reCAPTCHA response.
   */
  public verifyCaptcha() {
    this.captcha.status = true
    this.validateSubmit()
  }

  /**
   * 
   */
  public declinedCaptcha(){
    this.captcha.status = false
    this.validateSubmit()
  }

  /**
   * Delay Submit
   * Función creada para el caso de uso en que el usuario ha fallado >= 2 intentos.
   * Crea un delay de 30 segundos
   */
  public delaySubmit() {
    var cont = 30;
    var delay = this.delay.nativeElement;
    this.captcha.delay = true
    let id = setInterval(()=> {
      this.delay.nativeElement.innerHTML = 'Espera ' + cont.toString() + ' segundos para realizar el pago';
      cont--;
      if(cont == 0) {
        clearInterval(id);
        this.captcha.delay = false
        this.delay.nativeElement.innerHTML = '';
        this.validateSubmit()
      }
    }, 1000)

  }

  /**
   * Create Captcha
   */
  public createCaptcha() {
    if ( this.captcha.created == false ) {
      grecaptcha.render('captcha', {
        'sitekey'           : this.sitekey,
        'callback'          : 'verifyCaptcha',
        'expired-callback'  : 'declinedCaptcha',
        'error-callback'    : 'declinedCaptcha'
      });
      this.captcha.created = true
    } else {
      grecaptcha.reset();
    }
  }

  /**
   * Action captcha
   */
  public actionCaptcha () {
    if (this.captcha.attempts >= 1) {
      this.createCaptcha()
    }
    if (this.captcha.attempts >= 2){
      this.delaySubmit()
    }
    if (this.captcha.attempts >= 4){
      this.goHome()
    }
  }

  /**
   * Redirecciona a la pagina de inicio
   */
  goHome(): void {
    this.router.navigate([this.config.baseUrl]);
  }

  /**
   * Validate submit
   */
  public validateSubmit(): void{
    this.validate.emit(null)
  }
}
