import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service'
import { Subscription } from 'rxjs';

@Component({
  selector:"app-spinner",
  templateUrl: "./spinner.component.html",
  styleUrls: ['./spinner.component.sass']
})

export class spinnerComponent implements OnInit{

  public loading : boolean = false;
  public loadingSubscription : Subscription;

  /**
   * 
   * @param globalService 
   */
  constructor( private globalService : GlobalService){}

  /**
   * 
   */
  ngOnInit() {
    this.loadingSubscription = this.globalService.loading
      .subscribe( loading => {
        this.loading = loading
      });
  }

}