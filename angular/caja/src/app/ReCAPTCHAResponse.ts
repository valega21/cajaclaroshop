export interface ReCAPTCHAResponse {
  success: boolean; // whether this request was a valid reCAPTCHA token for your site
  status_code: number;
  error_codes?: Array<string>; // optional
}
